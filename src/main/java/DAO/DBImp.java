package DAO;

import DAO.interfacess.DBInterface;
import javafx.scene.control.Alert;
import org.hibernate.HibernateException;
import org.hibernate.JDBCException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import util.HibernateUtil;

import java.sql.SQLException;
import java.util.logging.Logger;

public class DBImp implements DBInterface {
    private Logger logger = Logger.getLogger(getClass().getName());

    @Override
    public void save(Object object) {
        SessionFactory factory = HibernateUtil.getSessionFactory();
        Session session = factory.getCurrentSession();
        try {
            if (session == null) {
                session = factory.openSession();
            }
            session.beginTransaction();
            session.save(object);
            session.getTransaction().commit();
        } catch (JDBCException e) {
            SQLException cause = (SQLException) e.getCause();
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setHeaderText("Podana nazwa programu już istnieje.");
            alert.setContentText(cause.getMessage());
            logger.info(e.getMessage());

        } finally {
            session.close();
        }
    }

    @Override
    public void update(Object object) {
        SessionFactory factory = HibernateUtil.getSessionFactory();
        Session session = factory.getCurrentSession();
        try {
            if (session == null) {
                session = factory.openSession();
            }
            session.beginTransaction();
            session.update(object);
        } catch (HibernateException e) {
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
        }
    }
}
