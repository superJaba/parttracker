package DAO;

import DAO.interfacess.InterfaceStrings;
import com.google.gson.Gson;
import model.ControlCart;
import model.CutPrograms;
import model.ElementCut;
import model.User;

import java.io.FileWriter;
import java.io.IOException;

public class MyFileWriter implements InterfaceStrings {
    private static ReadFromFile rff = new ReadFromFile();

    private static String path = rff.readConfigFromFile();

    private static Gson gson = new Gson();
    private static FileWriter fileWriter = null;

    public static void writeControlCartToFile(ControlCart controlCart) {
        try {
            fileWriter = new java.io.FileWriter(path + "\\carts.json", true);
            gson.toJson(controlCart, ControlCart.class, fileWriter);
            fileWriter.append("\n");
            fileWriter.close();
        } catch (IOException e) {
            System.out.println(ERROR_PROGRAM_TO_FILE + " kart kontrolnych.");
            e.printStackTrace();
        }

    }

    public static void writeToFileElement(ElementCut elementCut) {
        try {
            fileWriter = new java.io.FileWriter(path + PARTS_FILE_NAME, true);
            gson.toJson(elementCut, ElementCut.class, fileWriter);
            fileWriter.append("\n");
            fileWriter.close();
        } catch (IOException e) {
            System.out.println(ERROR_PROGRAM_TO_FILE);
            e.printStackTrace();
        }
    }

    public static void writeToFileCutPrograms(CutPrograms cutPrograms) {
        try {
            fileWriter = new java.io.FileWriter(path + PROGRAMS_FILE, true);
            gson.toJson(cutPrograms, fileWriter);
            fileWriter.append("\n");
            fileWriter.close();
        } catch (IOException e) {
            System.out.println(ERROR_PROGRAM_TO_FILE);
            e.printStackTrace();
        }
    }

    public static void writeToFile(String s) {
        try {
            fileWriter = new java.io.FileWriter(path + PROGRAMS_FILE, false);
            fileWriter.write(s);
            fileWriter.close();
        } catch (IOException e) {
            System.out.println("Blad zapisu do pliku");
        }
    }

    public void writeUserToFile(User user) {
        try {
            fileWriter = new FileWriter("C:\\db\\users.dat", true);
            gson.toJson(user, fileWriter);
            fileWriter.append("\n");
            fileWriter.close();
        } catch (IOException e) {
            System.out.println("Błąd zapisu do pliku urzytkowników.");
        }
    }

    public static void writeConfig() {
        String s = "C:\\db\\ustawienia\\config.txt";
        try {
            fileWriter = new java.io.FileWriter(s, true);
            fileWriter.write("C:\\db");
            fileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
