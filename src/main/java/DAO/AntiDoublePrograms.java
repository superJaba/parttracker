package DAO;

import model.CutPrograms;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AntiDoublePrograms {

    public Map<String, CutPrograms> mToMap() {
        Map<String, CutPrograms> cutProgramsMap = new HashMap<>();
        List<CutPrograms> cutProgramsList = new ArrayList<>();
        ReadFromFile readFromFile = new ReadFromFile();

        cutProgramsList.addAll(readFromFile.readAllProgramsToList());

        for (CutPrograms aCutProgramsList : cutProgramsList) {
            if (cutProgramsMap.containsKey(aCutProgramsList.getProgramName())) {
                System.out.println("Program: '" + aCutProgramsList.getProgramName() + "' jest juz w bazie.");
            } else {
                cutProgramsMap.put(aCutProgramsList.getProgramName(), aCutProgramsList);
            }
        }
        return cutProgramsMap;
    }

}
