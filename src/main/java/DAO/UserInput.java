package DAO;

import model.ElementCut;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;

/**
 * Created by Karamba on 2017-08-21.
 */
public class UserInput {

    private Scanner sc = new Scanner(System.in);
    private static DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    public String getUserInput() {
        return sc.nextLine();
    }

    public void setTimeOfCut(ElementCut elementCut) {
        LocalDateTime now = LocalDateTime.now();
        elementCut.setDateOfCut(dtf.format(now));
    }
}

