package DAO;

import DAO.interfacess.CutProgramsDBInterface;
import model.CutProgramsDB;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import util.HibernateUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class CutProgramsDBImplementation implements CutProgramsDBInterface {

    @Override
    public void deleteProgram(CutProgramsDB cutProgramsDB) {
        SessionFactory factory = HibernateUtil.getSessionFactory();
        Session session = factory.getCurrentSession();
        try {
            session.beginTransaction();
            session.delete(cutProgramsDB);
            session.getTransaction().commit();
        } catch (HibernateException e) {
            e.printStackTrace();
        } finally {
            session.close();
        }
    }

    @Override
    public List<CutProgramsDB> getAllCutPrograms() {
        List<CutProgramsDB> programsDBS = new ArrayList<>();
        SessionFactory factory1 = HibernateUtil.getSessionFactory();
        Session session1 = factory1.getCurrentSession();
        try {
            if (session1 == null) {
                System.out.println("MASZ NULLA SESJI W 'getAllCutPrograms'");
            }
            if (session1 != null) {
                session1.beginTransaction();
            }
            programsDBS = Objects.requireNonNull(session1).createQuery("from CutProgramsDB").list();

            session1.getTransaction().commit();
            session1.close();
        } catch (HibernateException e) {
            e.printStackTrace();
        }
        return programsDBS;
    }
}