package DAO;

import DAO.interfacess.InterfaceStrings;
import com.google.gson.Gson;
import model.CutPrograms;
import model.ElementCut;
import model.ElementCutOldVer;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

public class ReadFromFile implements InterfaceStrings {

    private String path = readConfigFromFile();


    private Gson gson = new Gson();

    public void readAllElements() {

        try (Stream<String> elementCutStream = Files.lines(Paths.get(path + PARTS_FILE_NAME))) {
            elementCutStream.forEach(System.out::println);

        } catch (IOException e) {
            System.out.println(InterfaceStrings.ERROR_READ_FILE);
            e.printStackTrace();
        }
    }

    public List<ElementCutOldVer> readAllElementsToList() {
        List<ElementCutOldVer> list = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(PARTS_FILE_NAME))) {
            String content;
            while ((content = br.readLine()) != null) {
                gson.getAdapter(ElementCutOldVer.class);
                ElementCutOldVer elementCut = gson.fromJson(content, ElementCutOldVer.class);
                list.add(elementCut);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return list;
    }


    public void searchElement(String name) {
        List<ElementCut> elementCutList = new ArrayList<>();
        List<ElementCut> temp = new ArrayList<>();

        try {
            BufferedReader br = new BufferedReader(new FileReader(path + PARTS_FILE_NAME));
            String content;
            while ((content = br.readLine()) != null) {
                gson.getAdapter(ElementCut.class);
                ElementCut elementCut = gson.fromJson(content, ElementCut.class);
                elementCutList.add(elementCut);
            }
            br.close();
            for (ElementCut anElementCutList : elementCutList) {
                if (anElementCutList.getPartNumber().contains(name)) {
                    temp.add(anElementCutList);
                }
            }
            for (ElementCut aTemp : temp) {
                System.out.println(aTemp.toString());
            }
            System.out.println();
        } catch (IOException e) {
            System.out.println(ERROR_PART_NOT_FOUND + name);
            e.printStackTrace();
        }
    }

    public List<CutPrograms> readAllProgramsToList() {
        List<CutPrograms> cutProgramsList = new ArrayList<>();
        String program;
        try (BufferedReader br = new BufferedReader(new FileReader(path + PROGRAMS_FILE))) {
            while ((program = br.readLine()) != null) {
                gson.getAdapter(CutPrograms.class);
                cutProgramsList.add(gson.fromJson(program, CutPrograms.class));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return cutProgramsList;
    }

    public void readAllPrograms() {
        List<CutPrograms> cutProgramsList = new ArrayList<>();
        String program;
        try (BufferedReader br = new BufferedReader(new FileReader(path + PROGRAMS_FILE))) {
            while ((program = br.readLine()) != null) {
                gson.getAdapter(CutPrograms.class);
                cutProgramsList.add(gson.fromJson(program, CutPrograms.class));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        mFullListToString(cutProgramsList);
    }

    private void mFullListToString(List<CutPrograms> cutProgramsList) {
        for (CutPrograms aCutPrograms : cutProgramsList) {
            System.out.println(aCutPrograms.toString());
        }
    }

    public void searchForEleInPrograms(String eName) {
        List<CutPrograms> cutProgramsList = new ArrayList<>();
        List<CutPrograms> temp = new ArrayList<>();

        mReadFromCutPrograms(eName, cutProgramsList);
        for (CutPrograms aCutProgramsList : cutProgramsList) {
            for (int j = 0; j < aCutProgramsList.getElementCutted().size(); j++) {
                if (aCutProgramsList.getElementCutted().get(j).getPartNumber().contains(eName)) {
                    temp.add(aCutProgramsList);
                }
            }
        }
        if (temp.isEmpty()) {
            System.out.println("\nNIE MA TAKIEGO ELEMENTU W BAZIE\n");
        } else {
            mFullListToString(temp);
        }
    }

    private void mReadFromCutPrograms(String eName, List<CutPrograms> cutProgramsList) {
        try (BufferedReader br = new BufferedReader(new FileReader(path + PROGRAMS_FILE))) {
            String cutContent;
            while ((cutContent = br.readLine()) != null) {
                gson.getAdapter(CutPrograms.class);
                CutPrograms cutPrograms = gson.fromJson(cutContent, CutPrograms.class);
                cutProgramsList.add(cutPrograms);
            }
        } catch (FileNotFoundException e) {
            System.out.println(ERROR_FILE_NOT_FOUND);
            e.printStackTrace();
        } catch (IOException e) {
            System.out.println(ERROR_PROG_NOT_FOUND + eName);
            e.printStackTrace();
        }
    }

    public List<CutPrograms> mSearchForProgram(String w) {
        List<CutPrograms> cutProgramsList = new ArrayList<>();
        List<CutPrograms> temp = new ArrayList<>();

        mReadFromCutPrograms(w, cutProgramsList);

        for (CutPrograms aCutProgramsList : cutProgramsList) {
            if (aCutProgramsList.getProgramName().toLowerCase().contains(w.toLowerCase())) {
                temp.add(aCutProgramsList);
            }
        }
        return temp;
    }


    public String readConfigFromFile() {


        try {
            BufferedReader br = new BufferedReader(new FileReader("C:\\db\\ustawienia\\config.txt"));
            path = br.readLine();
            br.close();
        } catch (IOException e) {
            System.out.println("Nie znaleziono pliku config.txt" + e);
            e.printStackTrace();
        }
        return path;
    }
}
