package DAO;

import DAO.interfacess.UserDBInterface;
import model.User;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import util.HibernateUtil;

import java.util.ArrayList;
import java.util.List;

public class UserDBImplementation implements UserDBInterface {

    @Override
    public User getUserByID(Integer userId) {
        User user = null;
        SessionFactory factory = HibernateUtil.getSessionFactory();
        try (Session session = factory.getCurrentSession()) {
            session.beginTransaction();
            String queryString = "from User where id = :id";
            Query query = session.createQuery(queryString);
            query.setInteger("id", userId);
            user = (User) query.uniqueResult();
        } catch (RuntimeException e) {
            e.printStackTrace();
        }
        return user;
    }

    @Override
    public void deleteUser(User user) {
        SessionFactory factory = HibernateUtil.getSessionFactory();
        try (Session session = factory.getCurrentSession()) {
            session.beginTransaction();
            session.delete(user);
            session.getTransaction().commit();
        } catch (HibernateException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<User> getAllUsers() {
        List<User> list = new ArrayList<>();
        SessionFactory factory = HibernateUtil.getSessionFactory();
        Session session = factory.getCurrentSession();
        try {
            if ( session == null ) {
                session = factory.openSession();
            }
            session.beginTransaction();
            list = session.createQuery("from User").list();
            session.getTransaction().commit();
            session.close();
        } catch (HibernateException e) {
            e.printStackTrace();
        }
        return list;
    }
}
