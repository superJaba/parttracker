package DAO;

import DAO.interfacess.PlateDBInterface;
import model.Plate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import util.HibernateUtil;

import java.util.ArrayList;
import java.util.List;

public class PlateDBImplementation implements PlateDBInterface {

    @Override
    public void deletePlate(Plate plate) {
        SessionFactory factory = HibernateUtil.getSessionFactory();
        try (Session session = factory.getCurrentSession()) {
            session.beginTransaction();
            session.delete(plate);
            session.getTransaction().commit();
        } catch (HibernateException e) {
            e.getMessage();
        }

    }

    @Override
    public List<Plate> plateList() {
        List<Plate> listFromDB = new ArrayList<>();
        SessionFactory factory = HibernateUtil.getSessionFactory();
        Session session = factory.getCurrentSession();
        try {
            session.beginTransaction();
            listFromDB.addAll(session.createQuery("from Plate").list());
        } catch (HibernateException e) {
            System.err.println(e);
        }
        session.getTransaction().commit();
        session.close();
        return listFromDB;
    }
}
