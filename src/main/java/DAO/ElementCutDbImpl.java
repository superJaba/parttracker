package DAO;

import DAO.interfacess.ElementCutDbInterface;
import model.ElementCut;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import util.HibernateUtil;

public class ElementCutDbImpl implements ElementCutDbInterface {
    @Override
    public void save(ElementCut elementCut) {
        SessionFactory factory = HibernateUtil.getSessionFactory();
        Session session = factory.getCurrentSession();
        session.beginTransaction();
        session.save(elementCut);
        session.close();
    }

    @Override
    public void delete(ElementCut elementCut) {

    }

    @Override
    public void findById(long id) {

    }
}
