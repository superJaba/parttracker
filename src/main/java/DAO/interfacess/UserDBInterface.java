package DAO.interfacess;

import model.User;

import java.util.List;

public interface UserDBInterface {

    User getUserByID(Integer userId);

    void deleteUser(User user);

    List getAllUsers();
}
