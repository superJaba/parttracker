package DAO.interfacess;

import model.Plate;

import java.util.List;

public interface PlateDBInterface {

    void deletePlate(Plate plate);

    List<Plate> plateList();
}
