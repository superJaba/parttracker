package DAO.interfacess;

import model.ElementCut;

public interface ElementCutDbInterface {

    void save(ElementCut elementCut);

    void delete(ElementCut elementCut);

    void findById(long id);


}
