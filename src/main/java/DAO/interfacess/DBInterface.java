package DAO.interfacess;


public interface DBInterface<T> {

    void save(T object);

    void update(T object);
}
