package DAO.interfacess;

/**
 * Created by Karamba on 2017-09-25.
 */
public interface InterfaceStrings {
    String ERROR_PROGRAM_TO_FILE = "cos poszlo nie tak podczas zapisu do pliku";
    String PARTS_FILE_NAME = "\\parts.json";
    String PROGRAMS_FILE = "\\programs.json";
    String ERROR_READ_FILE = "cos poszlo nie tak podczas odczytu z pliku";
    String ERROR_FILE_NOT_FOUND = "Nie odnaleziono pliku z programami";
    String ERROR_PROG_NOT_FOUND = "Coś poszło nie tak podczas szukania programu z elementem :";
    String ERROR_PART_NOT_FOUND = "Cos poszlo nie tak podczasz przeszukiwania pliku pod wzgledem ";
    String WRITED = "--Zapisano--\n";
}
