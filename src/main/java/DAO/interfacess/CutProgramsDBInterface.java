package DAO.interfacess;

import model.CutProgramsDB;

import java.util.List;

public interface CutProgramsDBInterface {

    void deleteProgram(CutProgramsDB cutProgramsDB);

    List<CutProgramsDB> getAllCutPrograms();
}
