package DAO.interfacess;

import model.ElementCut;

import java.util.List;

public interface ElementDBInterface {
    void deleteElement(ElementCut elementCut);

    ElementCut getElementByID(Integer id);

    List<ElementCut> getAllElements();
}
