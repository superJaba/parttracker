package DAO;

import DAO.interfacess.ElementDBInterface;
import model.ElementCut;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import util.HibernateUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class ElementDBImplementation implements ElementDBInterface {


    @Override
    public void deleteElement(ElementCut elementCut) {
        SessionFactory factory = HibernateUtil.getSessionFactory();
        Session session = factory.getCurrentSession();
        try {
            session.beginTransaction();
            session.delete(elementCut);
            session.getTransaction().commit();
        } catch (HibernateException e) {
            e.printStackTrace();
        } finally {
            session.close();
        }
    }

    @Override
    public ElementCut getElementByID(Integer id) {
        SessionFactory factory = HibernateUtil.getSessionFactory();
        Session session = factory.getCurrentSession();
        session.beginTransaction();
        ElementCut elementCut = session.byId(ElementCut.class).load(id);

        session.getTransaction().commit();
        session.close();

        return elementCut;
    }

    @Override
    public List getAllElements() {
        List elementCutList = new ArrayList();
        try {
            SessionFactory factory1 = HibernateUtil.getSessionFactory();
            Session session1 = factory1.getCurrentSession();
            if (session1 == null) {
                System.out.println("MASZ NULLA SESJI W getAllElements");
            }
            if (session1 != null) {
                session1.beginTransaction();
            }
            elementCutList = Objects.requireNonNull(session1).createQuery("from ElementCut").list();
            session1.getTransaction().commit();
            session1.close();
        } catch (Throwable e) {
            e.printStackTrace();
        }
        return elementCutList;
    }
}
