package controllers;

import DAO.DBImp;
import DAO.UserInput;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.stage.Stage;
import javafx.util.StringConverter;
import javafx.util.converter.IntegerStringConverter;
import model.CutProgramsDB;
import model.ElementCut;
import model.ElementCutForProgramsDB;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class CheckCutedProgramController implements Initializable {

    @FXML    private TableView<ElementCut> elementsTable;
    @FXML    private TableColumn<ElementCut, String> elementName;
    @FXML    private TableColumn<ElementCut, Integer> elementAmount;
    @FXML    private TableColumn<ElementCut, String> elementDesc;
    @FXML    private TableColumn<ElementCut, String> elementDateOfCut;
    @FXML    private TableColumn<ElementCut, String> elementMachine;
    @FXML    private Button buttonOK;
    @FXML    private Button buttonCancel;

    private Integer sheetAmount = null;

    private CutProgramsDB cutProgramsDB = null;
    private List<ElementCutForProgramsDB> elementCutForProgramsDBList = new ArrayList<>();
    private ObservableList<ElementCut> elementCutObservableList = FXCollections.observableArrayList();

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        elementCutForProgramsDBList.addAll(cutProgramsDB.getElementCutList());
        //TODO load data from DB in new thread
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
            }
        });
        objectFieldSwapping();
        fabricForTableCell();
        buttonOK.setOnAction(event -> onOKButtonPress());
        buttonCancel.setOnAction(event -> onCancelButtonPress());
    }

    private void objectFieldSwapping() {
        for (ElementCutForProgramsDB elementCutForProgramsDB : elementCutForProgramsDBList) {
            ElementCut elementCut = new ElementCut();
            elementCut.setPartNumber(elementCutForProgramsDB.getPartNumber());
            elementCut.setPartAmount(elementCutForProgramsDB.getPartAmount() * sheetAmount);
            elementCut.setMachine(cutProgramsDB.getMaschine());
            UserInput userInput = new UserInput();
            userInput.setTimeOfCut(elementCut);
            elementCut.setDescription("");
            elementCutObservableList.add(elementCut);
        }
    }

    private void fabricForTableCell() {
        elementName.setCellValueFactory(new PropertyValueFactory<>("partNumber"));
        elementName.setCellFactory(TextFieldTableCell.forTableColumn());

        elementAmount.setCellValueFactory(new PropertyValueFactory<>("partAmount"));
        StringConverter<Integer> stringConverter = new IntegerStringConverter();
        elementAmount.setCellFactory(TextFieldTableCell.forTableColumn(stringConverter));
        elementAmount.setOnEditCommit(event -> event.getTableView().getItems().get(
                event.getTablePosition().getRow()).setPartAmount(event.getNewValue()));

        elementDesc.setCellValueFactory(new PropertyValueFactory<>("description"));
        elementDesc.setCellFactory(TextFieldTableCell.forTableColumn());
        elementDesc.setOnEditCommit(event -> event.getTableView().getItems().get(
                event.getTablePosition().getRow()).setDescription(event.getNewValue()));

        elementDateOfCut.setCellValueFactory(new PropertyValueFactory<>("dateOfCut"));
        elementDateOfCut.setCellFactory(TextFieldTableCell.forTableColumn());
        elementDateOfCut.setOnEditCommit(event -> event.getTableView().getItems().get(
                event.getTablePosition().getRow()).setDateOfCut(event.getNewValue()));

        elementMachine.setCellValueFactory(new PropertyValueFactory<>("machine"));
        elementMachine.setCellFactory(TextFieldTableCell.forTableColumn());

        elementsTable.getItems().clear();
        elementsTable.getItems().addAll(elementCutObservableList);
        elementsTable.refresh();
    }

    private void onOKButtonPress() {
        DBImp dbImp = new DBImp();
        for (ElementCut elementCut : elementCutObservableList) {
            dbImp.save(elementCut);
        }
        getPlate();// dopasowanie blachy z programu ciecia do blachy z DB

        windowClose(buttonOK);
    }

    private void getPlate() {
        String[] sheetDimensionArray = cutProgramsDB.getSheetDimensions().split("x");
        String[] sheetThicknesArray = cutProgramsDB.getSheetThicknes().split(" ");
    }

    private void onCancelButtonPress() {
        windowClose(buttonCancel);
    }

    private void windowClose(Button button) {
        Stage stage = (Stage) button.getScene().getWindow();
        stage.close();
    }

    public void setCutProgramsDB(CutProgramsDB cutProgramsDB) {
        this.cutProgramsDB = cutProgramsDB;
    }

    public void setSheetAmount(Integer sheetAmount) {
        this.sheetAmount = sheetAmount;
    }
}
