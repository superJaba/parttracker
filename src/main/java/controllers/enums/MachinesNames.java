package controllers.enums;

public enum MachinesNames {
    SPACEGEAR("SpaceGear 2.5kW"),
    SUPERTURBOx510("SuperTurbo X510 4kW"),
    NEWTURBO("New Turbo X48 1.5kW"),
    TRUMPF1000("TruPunch 1000"),
    GRUBSON("Trumatic 600L");

    private String string;

    MachinesNames(String string) {
        this.string = string;
    }

    public String MachineNames() {
        return string;
    }

    @Override
    public String toString() {
        return string;
    }
}
