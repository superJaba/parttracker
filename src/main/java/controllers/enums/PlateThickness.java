package controllers.enums;

public enum PlateThickness {
    A(0.5),
    B(0.8),
    C(1),
    D(1.2),
    E(1.25),
    F(1.5),
    G(2),
    H(2.5),
    I(3.0),
    J(4),
    K(5),
    L(6),
    M(7),
    N(8),
    O(10),
    P(12),
    Q(15),
    R(16),
    S(18),
    T(20);

    private double number;

    PlateThickness(double v) {
        this.number = v;
    }

    public double getNumber() {
        return number;
    }

    @Override
    public String toString() {
        return String.valueOf(number);
    }
}
