package controllers.enums;

public enum InoxPlateType implements PlateType {
    A("304 SZCZOTKA"),
    B("304"),
    C("1.4003"),
    D("1.4003 LUSTRO");

    private String string;

    InoxPlateType(String string) {
        this.string = string;
    }

    @Override
    public String toString() {
        return string;
    }
}
