package controllers.enums;

public enum AlmgPlateType implements PlateType {
    A("5754"),
    B("5754 (H111)"),
    C("Ryfel");

    private String string;

    AlmgPlateType(String string) {
        this.string = string;
    }

    @Override
    public String toString() {
        return string;
    }
}
