package controllers.enums;

public enum PlateMaterials {
    STAL("STAL"),
    ALUMINIUM("ALUMINIUM"),
    INOX("INOX"),
    OCYNK("OCYNK"),
    DOMEX("DOMEX"),
    WELDOX("WELDOX");

    private String string;

    PlateMaterials(String string) {
        this.string = string;
    }

    @Override
    public String toString() {
        return string;
    }
}
