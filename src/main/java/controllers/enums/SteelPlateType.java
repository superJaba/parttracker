package controllers.enums;

public enum SteelPlateType  implements PlateType {
    //stal
    A("Z/W"),
    B("SAAB"),
    C("G/W"),
    D("S 355 MC"),
    E("S 420 MC"),
    F("S 650 MC"),
    G("S 500 MC"),
    H("S 460 MC"),
    I("S 700 MC");

    private String string;

    SteelPlateType(String string) {
        this.string = string;
    }

    @Override
    public String toString() {
        return string;
    }
}
