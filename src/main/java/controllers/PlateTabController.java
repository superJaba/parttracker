package controllers;

import DAO.DBImp;
import DAO.PlateDBImplementation;
import controllers.enums.*;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.util.Callback;
import javafx.util.StringConverter;
import javafx.util.converter.DoubleStringConverter;
import javafx.util.converter.IntegerStringConverter;
import model.Plate;

import java.net.URL;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;

public class PlateTabController implements Initializable {
    @FXML    private AnchorPane anchorPaneTabFillFXID;
    @FXML    private TableView<Plate> platesTable;
    @FXML    private TableColumn<Plate, String> columnMaterial;
    @FXML    private TableColumn<Plate, Double> columnThickness;
    @FXML    private TableColumn<Plate, String> columnType;
    @FXML    private TableColumn<Plate, Integer> columnDimX;
    @FXML    private TableColumn<Plate, Integer> columnDimY;
    @FXML    private TableColumn<Plate, Integer> columnAmount;
    @FXML    private TableColumn<Plate, Integer> columnReservedAmount;
    @FXML    private TableColumn<Plate, String> columnDesc;
    @FXML    private TableColumn<Plate, String> columnButDelete;
    @FXML    private ComboBox<PlateMaterials> materialsComboBox;
    @FXML    private ComboBox<PlateThickness> thicknessComboBox;
    @FXML    private HBox plateTypeHBox;
    @FXML    private TextField amountTF;
    @FXML    private TextField xTextField;
    @FXML    private TextField yTextField;
    @FXML    private TextField descTextField;
    @FXML    private Button addNewButton;
    @FXML    private Button refreshButton;
    private List<Plate> plateList = FXCollections.observableArrayList();
    private String valueOfPlateTypeComboBox = null;
    private boolean flag = false;
    private ComboBox<String> dynamicComboBox = new ComboBox<>();

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        materialsComboBox.getItems().addAll(Arrays.asList(PlateMaterials.values()));
        thicknessComboBox.getItems().addAll(Arrays.asList(PlateThickness.values()));
        dynamicComboBox.setEditable(true);
        dynamicComboBox.setPromptText("Typ materiału");
        plateTypeHBox.getChildren().add(dynamicComboBox);


        fillTableWithData();
        getDataFromDB();

        addNewButton.setOnAction(event -> {
            Plate plate = new Plate();
            plate.setSubstance(String.valueOf(materialsComboBox.getValue()));
            plate.setThickness(Double.valueOf(String.valueOf(thicknessComboBox.getValue())));
            plate.setType(valueOfPlateTypeComboBox);
            plate.setDimX(Integer.valueOf(xTextField.getText()));
            plate.setDimY(Integer.valueOf(yTextField.getText()));
            plate.setAmount(Integer.valueOf(amountTF.getText()));
            plate.setDescription(descTextField.getText());
            plate.setReservation(0);
            DBImp db = new DBImp();
            db.save(plate);
            clearFields();
            platesTable.getItems().add(plate);
            platesTable.refresh();
        });

        refreshButton.setOnAction(event -> {
            platesTable.getItems().clear();
            platesTable.getItems().addAll(getDataFromDB());
            platesTable.refresh();
            clearFields();
        });
    }

    private void loadComboBox() {
        if (flag) {
            dynamicComboBox.getItems().clear();
            String materialType = String.valueOf(materialsComboBox.getValue());
            plateTypeHBox.setVisible(true);
            String[] comboName;
            PlateType[] plateType = null;
            switch (materialType) {
                case "ALUMINIUM":
                    plateType = AlmgPlateType.values();
                    comboName = new String[plateType.length];
                    break;
                case "INOX":
                    plateType = InoxPlateType.values();
                    comboName = new String[plateType.length];
                    break;
                default:
                    plateType = SteelPlateType.values();
                    comboName = new String[plateType.length];
                    break;
            }
            for (int i = 0; i < plateType.length; i++) {
                comboName[i] = plateType[i].toString();
            }
            dynamicComboBox.getItems().addAll(comboName);
            observerForComboBox();
        }
    }

    private void observerForComboBox() {
        dynamicComboBox.valueProperty()
                .addListener((observable, oldValue, newValue) ->
                {
                    if (newValue != null) {
                        valueOfPlateTypeComboBox = newValue;
                    }
                });
    }

    private List<Plate> getDataFromDB() {
        PlateDBImplementation plateDBImplementation = new PlateDBImplementation();
        return plateDBImplementation.plateList();
    }

    private void clearFields() {
        amountTF.clear();
        xTextField.clear();
        yTextField.clear();
        descTextField.clear();
        materialsComboBox.setValue(null);
        thicknessComboBox.setValue(null);
        dynamicComboBox.setValue(null);
    }

    private void fillTableWithData() {
        platesTable.getItems().addAll(getDataFromDB());
        columnMaterial.setCellValueFactory(new PropertyValueFactory<>("substance"));
        columnMaterial.setCellFactory(TextFieldTableCell.forTableColumn());

        columnThickness.setCellValueFactory(new PropertyValueFactory<>("thickness"));
        StringConverter<Double> doubleStringConverter = new DoubleStringConverter();
        columnThickness.setCellFactory(TextFieldTableCell.forTableColumn(doubleStringConverter));

        columnType.setCellValueFactory(new PropertyValueFactory<>("type"));
        columnType.setCellFactory(TextFieldTableCell.forTableColumn());

        columnDimX.setCellValueFactory(new PropertyValueFactory<>("dimX"));
        StringConverter<Integer> integerStringConverter = new IntegerStringConverter();
        columnDimX.setCellFactory(TextFieldTableCell.forTableColumn(integerStringConverter));
        columnDimX.setOnEditCommit(event -> {
            event.getTableView().getItems().get(event.getTablePosition().getRow()).setDimX(event.getNewValue());
            valueUpdate(event);
        });

        columnDimY.setCellValueFactory(new PropertyValueFactory<>("dimY"));
        columnDimY.setCellFactory(TextFieldTableCell.forTableColumn(integerStringConverter));
        columnDimY.setOnEditCommit(event -> {
            event.getTableView().getItems().get(event.getTablePosition().getRow()).setDimY(event.getNewValue());
            valueUpdate(event);
        });

        columnAmount.setCellValueFactory(new PropertyValueFactory<>("amount"));
        columnAmount.setCellFactory(TextFieldTableCell.forTableColumn(integerStringConverter));
        columnAmount.setOnEditCommit(event -> {
            event.getTableView().getItems().get(event.getTablePosition().getRow()).setAmount(event.getNewValue());
            valueUpdate(event);
        });

        columnReservedAmount.setCellValueFactory(new PropertyValueFactory<>("reservation"));
        columnReservedAmount.setCellFactory(TextFieldTableCell.forTableColumn(integerStringConverter));

        columnDesc.setCellValueFactory(new PropertyValueFactory<>("description"));
        columnDesc.setCellFactory(TextFieldTableCell.forTableColumn());
        columnDesc.setOnEditCommit(event -> {
            event.getTableView().getItems().get(event.getTablePosition()
                    .getRow()).setDescription(event.getNewValue());
            valueUpdate(event);
        });

        columnButDelete.setCellValueFactory(new PropertyValueFactory<>("Button"));
        Callback<TableColumn<Plate, String>, TableCell<Plate, String>> cellFactory =
                new Callback<TableColumn<Plate, String>, TableCell<Plate, String>>() {
                    @Override
                    public TableCell<Plate, String> call(TableColumn<Plate, String> param) {
                        return new TableCell<Plate, String>() {
                            final Button button = new Button("Usuń");

                            @Override
                            public void updateItem(String item, boolean empty) {
                                super.updateItem(item, empty);
                                if (empty) {
                                    setGraphic(null);
                                    setText(null);
                                } else {
                                    button.setOnAction(event -> {
                                        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                                        alert.setTitle("Zatwierdzenie zmian.");
                                        alert.setHeaderText("Przy wybraniu OK nastąpi usunięcie balchy.");
                                        alert.setContentText("Kontynuować?");

                                        Optional<ButtonType> result = alert.showAndWait();
                                        if (result.get() == ButtonType.OK) {
                                            Plate plateDelete = getTableView().getItems().get(getIndex());
                                            PlateDBImplementation db = new PlateDBImplementation();


                                            db.deletePlate(plateDelete);
                                            platesTable.getItems().remove(plateDelete);
                                            platesTable.refresh();
                                        } else {
                                            alert.close();
                                        }
                                    });
                                    setGraphic(button);
                                    setText(null);
                                }
                            }

                        };
                    }
                };
        columnButDelete.setCellFactory(cellFactory);
    }

    private void valueUpdate(TableColumn.CellEditEvent<?, ?> event) {
        DBImp db = new DBImp();
        db.update(event.getRowValue());
    }

    public void triger1(MouseEvent mouseEvent) {
        flag = true;
        loadComboBox();
    }
}
