package controllers;

import DAO.CutProgramsDBImplementation;
import DAO.DBImp;
import DAO.ElementDBImplementation;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Callback;
import javafx.util.StringConverter;
import javafx.util.converter.DateStringConverter;
import javafx.util.converter.IntegerStringConverter;
import model.CutProgramsDB;
import model.ElementCut;
import model.ElementCutForProgramsDB;
import model.User;

import java.io.IOException;
import java.net.URL;
import java.util.*;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;

public class ProgramController implements Initializable {
    public ProgramController() {
    }

    @FXML
    private Tab ordersTab;
    @FXML
    private Tab idUserTab;
    @FXML
    private Tab idPlate;
    @FXML
    private PlateTabController anchorPaneTabFill;


    //kontrolki Programs TabView
    @FXML
    private TableView<CutProgramsDB> tablePrograms;
    @FXML
    private TableColumn<CutProgramsDB, String> cellProgramName;
    @FXML
    private TableColumn<CutProgramsDB, Date> cellDateGeneration;
    @FXML
    private TableColumn<CutProgramsDB, String> cellMaterialThicknes;
    @FXML
    private TableColumn<CutProgramsDB, String> cellSheetDimensions;
    @FXML
    private TableView<ElementCutForProgramsDB> elementsTableView;
    @FXML
    private TableColumn<CutProgramsDB, String> machineProgramsTable;
    @FXML
    private TableColumn<CutProgramsDB, String> cellOtionsProgramTableView;
    @FXML
    private TableColumn<ElementCutForProgramsDB, String> elementInProgramName;
    @FXML
    private TableColumn<ElementCutForProgramsDB, Integer> elementInProgramAmount;
    @FXML
    private TextField programSearchTextArea;
    @FXML
    private TextField elementSearchInPrograms;
    @FXML
    private Button buttonRefreshProgramTable;
    @FXML
    private Button buttonNewProgram;
    @FXML
    private Button programXNumberOfSheets;

    private final ObservableList<CutProgramsDB> observableList = FXCollections.observableArrayList();
    private final ObservableList<ElementCut> elementCutObservableList = FXCollections.observableArrayList();
    private User loggedUser;
    private Stage windowOwner;
    private Integer rowIndex;


    @Override
    public void initialize(URL location, ResourceBundle resources) {

        fillProgramTabs(); //fabryka wypelniajaca tabele programow do cieca
        update();//watek z zaladowaniem danych na nowo


        //kontrola widoku zaleznie od urzytkownika
        if ((getLoggedUser().getUserGroup().equals("superAdmin")) || (getLoggedUser().getUserGroup().equals("biuro"))) {
            idUserTab.setDisable(false);
            idUserTab.getTabPane().getSelectionModel().select(2);
        } else {
            idUserTab.setDisable(true);
        }

        controlsForProgramsTab();
    }


    //obsluga pol tekstowych i przyciskow w ProgramTableView
    private void controlsForProgramsTab() {

        buttonRefreshProgramTable.setTooltip(new Tooltip("Wczytuje ponownie programy z bazy."));
        programSearchTextArea.setTooltip(new Tooltip("Wyszukuje podaną nazwę programu\n w zbiorze dostępnych programów."));
        elementSearchInPrograms.setTooltip(new Tooltip("Wyszukuję potrzebny element w zbiorze dostępnych programów."));
        programXNumberOfSheets.setTooltip(new Tooltip("Najpierw zaznacz program, który został wycięty."));

        //przeladowuje tabele programow danymi z DB
        buttonRefreshProgramTable.setOnAction(event -> {
            buttonRefreshProgramTable.focusTraversableProperty().setValue(true);
            clearProgramTextFields();
            tablePrograms.getItems().clear();
            tablePrograms.getItems().addAll(getMyPrograms());
            tablePrograms.refresh();
        });

        //przemnozenie elementow w CutProgramDB x liczba blach
        programXNumberOfSheets.setOnAction(event -> {
            CutProgramsDB cutProgramsDB = tablePrograms.getItems().get(tablePrograms.getSelectionModel().getFocusedIndex());
            int sheetNumber = 0;
            try {
                TextInputDialog dialog = new TextInputDialog();
                dialog.setTitle("Podaj liczbę wyciętych arkuszy.");
                dialog.setHeaderText("");
                dialog.setContentText("Wprowadź liczbę:");

                Optional<String> result = dialog.showAndWait();
                sheetNumber = Integer.parseInt(result.get());

                String filePath2 = "/fxmlFiles/checkCutedProgram.fxml";
                String stageTitle = "Karambix - program x ilość arkuszy";
                CheckCutedProgramController controller = new CheckCutedProgramController();
                controller.setCutProgramsDB(cutProgramsDB);
                controller.setSheetAmount(sheetNumber);
                FXMLLoader loader = new FXMLLoader();
                loader.setLocation(this.getClass().getResource(filePath2));
                loader.setController(controller);
                AnchorPane anchorPane = null;
                try {
                    anchorPane = loader.load();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                Scene scene = new Scene(anchorPane);
                Stage modalStage = new Stage();
                modalStage.setScene(scene);
                modalStage.initModality(Modality.WINDOW_MODAL);
                modalStage.initOwner(windowOwner);
                modalStage.setTitle(stageTitle);
                modalStage.show();
            } catch (NoSuchElementException ignored) {
            }


            
        });

        Consumer<CutProgramsDB> onComplete = result -> {
//            tablePrograms.getItems().add(result);
            tablePrograms.refresh();
        };


        //załadowanie nowego widoku w wyskakujacym oknie i wypelnienie nowego obiektu CutProgramsDB danymi
        buttonNewProgram.setOnAction(event -> {
            NewCutProgramController ncpc = new NewCutProgramController();
            ncpc.setList(observableList);
            String fxmlFilePath = "/fxmlFiles/newPOJO/newCutProgram.fxml";
            String stageTitle = "Karambix newProgram";
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(this.getClass().getResource(fxmlFilePath));
            AnchorPane anchorPane = null;
            try {
                anchorPane = loader.load();
            } catch (IOException e) {
                e.printStackTrace();
            }
            NewCutProgramController controller = loader.getController();
            controller.initData(observableList, onComplete);
            assert anchorPane != null;
            Scene scene = new Scene(anchorPane);
            Stage modalStage = new Stage();
            modalStage.setScene(scene);
            modalStage.initModality(Modality.WINDOW_MODAL);
            modalStage.initOwner(windowOwner);
            modalStage.setTitle(stageTitle);
            modalStage.show();
        });

        //wyszukuje program o zadanej nazwie i odswieza widok w tabeli
        programSearchTextArea.textProperty().addListener((observable, oldValue, newValue) -> {
            tablePrograms.getItems().clear();
            for (CutProgramsDB anObservableList : observableList) {
                if (anObservableList.getProgramName().contains(observable.getValue())) {
                    tablePrograms.getItems().addAll(anObservableList);
                    tablePrograms.refresh();
                }
            }
        });

        //wyszukanie elementu znajdującego sie w liscie programów do ciecia i odswieza widok w tabeli
        elementSearchInPrograms.textProperty().addListener((observable1, oldValue, newValue) -> {
            tablePrograms.getItems().clear();
            for (CutProgramsDB anList : observableList) {
                for (ElementCutForProgramsDB cut : anList.getElementCutList())
                    if (cut.getPartNumber().contains(observable1.getValue())) {
                        tablePrograms.getItems().addAll(anList);
                        tablePrograms.refresh();
                    }
            }
        });

    }

    private void clearProgramTextFields() {
        programSearchTextArea.clear();
        elementSearchInPrograms.clear();
        elementsTableView.getItems().clear();
    }

    private List<ElementCut> getMeCuteddElements() {
        ElementDBImplementation elementDB = new ElementDBImplementation();
        elementCutObservableList.clear();
        elementCutObservableList.addAll(elementDB.getAllElements());
        return elementCutObservableList;
    }

    private void fillProgramTabs() {
        tablePrograms.setEditable(true);
        elementsTableView.setEditable(true);


        cellProgramName.setCellValueFactory(new PropertyValueFactory<>("programName"));
        cellProgramName.setCellFactory(TextFieldTableCell.forTableColumn());
        cellProgramName.setOnEditCommit(event -> {
            event.getTableView().getItems().get(
                    event.getTablePosition().getRow()).setProgramName(event.getNewValue());
            dbUpdateProgramT(event);
        });

        cellDateGeneration.setCellValueFactory(new PropertyValueFactory<>("dateOfGeneration"));
        StringConverter<Date> stringConverter = new DateStringConverter();
        cellDateGeneration.setCellFactory(TextFieldTableCell.forTableColumn(stringConverter));
        cellDateGeneration.setOnEditCommit(event -> {
            event.getTableView().getItems().get(
                    event.getTablePosition().getRow()).setDateOfGeneration(event.getNewValue());
            DBImp dbImp = new DBImp();
            dbImp.update(event.getRowValue());
        });

        cellMaterialThicknes.setCellValueFactory(new PropertyValueFactory<>("sheetThicknes"));
        cellMaterialThicknes.setCellFactory(TextFieldTableCell.forTableColumn());
        cellMaterialThicknes.setOnEditCommit(event -> {
            event.getTableView().getItems().get(
                    event.getTablePosition().getRow()).setSheetThicknes(event.getNewValue());
            dbUpdateProgramT(event);
        });

        cellSheetDimensions.setCellValueFactory(new PropertyValueFactory<>("sheetDimensions"));
        cellSheetDimensions.setCellFactory(TextFieldTableCell.forTableColumn());
        cellSheetDimensions.setOnEditCommit(event -> {
            event.getTableView().getItems().get(
                    event.getTablePosition().getRow()).setSheetDimensions(event.getNewValue());
            dbUpdateProgramT(event);
        });

        machineProgramsTable.setCellValueFactory(new PropertyValueFactory<>("maschine"));
        machineProgramsTable.setCellFactory(TextFieldTableCell.forTableColumn());
        machineProgramsTable.setOnEditCommit(event -> {
            event.getTableView().getItems().get(
                    event.getTablePosition().getRow()).setSheetDimensions(event.getNewValue());
            dbUpdateProgramT(event);
        });

        cellOtionsProgramTableView.setCellValueFactory(new PropertyValueFactory<>("PFFF"));
        Callback<TableColumn<CutProgramsDB, String>, TableCell<CutProgramsDB, String>> cellFactory2
                = new Callback<TableColumn<CutProgramsDB, String>, TableCell<CutProgramsDB, String>>() {
            @Override
            public TableCell call(final TableColumn<CutProgramsDB, String> param) {
                return new TableCell<CutProgramsDB, String>() {

                    final Button btn2 = new Button("Usuń");

                    @Override
                    public void updateItem(String item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty) {
                            setGraphic(null);
                            setText(null);
                        } else {
                            btn2.setOnAction(event -> {
                                Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                                alert.setTitle("Zatwierdzenie zmian");
                                alert.setHeaderText("Przy wybraniu OK nastąpi usunięcie programu.");
                                alert.setContentText("Kontynuować?");

                                Optional<ButtonType> result = alert.showAndWait();
                                if (result.get() == ButtonType.OK) {
                                    CutProgramsDB cutProgramsDB = getTableView().getItems().get(getIndex());
                                    CutProgramsDBImplementation db = new CutProgramsDBImplementation();

                                    clearProgramTextFields();
                                    tablePrograms.getItems().clear();
                                    db.deleteProgram(cutProgramsDB);
                                    tablePrograms.getItems().addAll(getMyPrograms());
                                    tablePrograms.refresh();
                                } else {
                                    alert.close();
                                }
                            });
                            setGraphic(btn2);
                            setText(null);
                        }
                    }
                };
            }
        };
        cellOtionsProgramTableView.setCellFactory(cellFactory2);
        tablePrograms.getItems().addAll(Objects.requireNonNull(getMyPrograms()));


        tablePrograms.setOnMouseClicked(event -> {
            rowIndex = tablePrograms.getSelectionModel().getSelectedIndex();
            elementInProgramName.setCellValueFactory(new PropertyValueFactory<>("partNumber"));
            elementInProgramName.setCellFactory(TextFieldTableCell.forTableColumn());
            elementInProgramName.setOnEditCommit(event1 -> {
                event1.getTableView().getItems().get(
                        event1.getTablePosition().getRow()).setPartNumber(event1.getNewValue());
                DBImp dbImp = new DBImp();
                dbImp.update(event1.getRowValue());
            });

            elementInProgramAmount.setCellValueFactory(new PropertyValueFactory<>("partAmount"));
            StringConverter<Integer> stringConvertersubElements = new IntegerStringConverter();
            elementInProgramAmount.setCellFactory(TextFieldTableCell.forTableColumn(stringConvertersubElements));
            elementInProgramAmount.setOnEditCommit(event1 -> {
                event1.getTableView().getItems().get(
                        event1.getTablePosition().getRow()).setPartAmount(Integer.valueOf(String.valueOf(event1.getNewValue())));
                DBImp dbImp = new DBImp();
                dbImp.update(event1.getRowValue());
            });
            elementsTableView.getItems().clear();
            elementsTableView.getItems().addAll(getMyElements(rowIndex));
        });

        tablePrograms.getItems().addAll(getMyPrograms());
        update();
    }

    private void dbUpdateProgramT(TableColumn.CellEditEvent<CutProgramsDB, String> event) {
        DBImp dbImp = new DBImp();
        dbImp.update(event.getRowValue());
    }

    private void update() {
        final ScheduledExecutorService service = Executors.newScheduledThreadPool(1);
        service.scheduleAtFixedRate(this::getMyPrograms, 600, 600, TimeUnit.SECONDS);
    }


    //TODO wright test
    private List<ElementCutForProgramsDB> getMyElements(int index) {
        ObservableList<CutProgramsDB> obsList = tablePrograms.getItems();
        if (obsList != null && obsList.size() != 0) {
            List<ElementCutForProgramsDB> elementCutted = tablePrograms.getItems().get(index).getElementCutList();
            ObservableList<ElementCutForProgramsDB> list = FXCollections.observableArrayList();
            list.addAll(elementCutted);
            return list;
        }
        return null;
    }

    //TODO wright test
    private List<CutProgramsDB> getMyPrograms() {
        tablePrograms.getItems().clear();
        observableList.clear();
        CutProgramsDBImplementation cutProgramsDBImplementation = new CutProgramsDBImplementation();
        observableList.addAll(cutProgramsDBImplementation.getAllCutPrograms());
        tablePrograms.getItems().addAll(observableList);
        tablePrograms.refresh();
        return observableList;
    }


    private User getLoggedUser() {
        return loggedUser;
    }

    public void setLoggedUser(User loggedUser) {
        this.loggedUser = loggedUser;
    }

    public void setWindowOwner(Stage windowOwner) {
        this.windowOwner = windowOwner;
    }

}

