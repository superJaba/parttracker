package controllers;

import DAO.DBImp;
import DAO.UserDBImplementation;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.util.Callback;
import model.User;
import org.hibernate.HibernateException;

import java.net.URL;
import java.util.*;

public class UserTabViewController implements Initializable {

    @FXML    private TableView<User> userTableView;
    @FXML    private TableColumn<User, String> userTableNameColumn;
    @FXML    private TableColumn<User, String> userTableLastnameColumn;
    @FXML    private TableColumn<User, String> userTableLoginColumn;
    @FXML    private TableColumn<User, String> userTablePasswordColumn;
    @FXML    private TableColumn<User, String> userTableEmailColumn;
    @FXML    private TableColumn<User, String> userTableGroupColumn;
    @FXML    private TableColumn<User, String> userTableOptions;
    @FXML    private TextField insertFirstnameTextFild;
    @FXML    private TextField insertLastnameTextField;
    @FXML    private TextField insertLoginTextField;
    @FXML    private TextField insertPassTextField;
    @FXML    private TextField insertEmailTextField;
    @FXML    private TextField insertWorkgroupTextField;
    @FXML    private Button addNewUserButtonUsersTab;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        fillUserTab(); //fabryka wypelniajaca tabele urzytkownikow
        controlsForUsersTab();

    }

    private void controlsForUsersTab() {
        addNewUserButtonUsersTab.setOnAction(event -> {
            String insertedFirstName = insertFirstnameTextFild.getText();
            String insertedLastName = insertLastnameTextField.getText();
            String insertedLogin = insertLoginTextField.getText();
            String insertedPass = insertPassTextField.getText();
            String insertedEmail = insertEmailTextField.getText();
            String insertedUserGroup = insertWorkgroupTextField.getText();
            userInputValidationForNewUser(insertedFirstName, insertedLastName, insertedLogin, insertedPass, insertedEmail, insertedUserGroup);
        });
    }

    private void userInputValidationForNewUser(String firstName, String lastName, String login, String pass, String email, String userGroup) {
        String emptyTextFields = "To pole nie może być puste!";
        if (firstName.isEmpty()) {
            insertFirstnameTextFild.clear();
            insertFirstnameTextFild.setPromptText(emptyTextFields);
        } else if (lastName.isEmpty()) {
            insertLastnameTextField.clear();
            insertLastnameTextField.setPromptText(emptyTextFields);
        } else if (login.isEmpty()) {
            insertLoginTextField.clear();
            insertLoginTextField.setPromptText(emptyTextFields);
        } else if (pass.isEmpty()) {
            insertPassTextField.clear();
            insertPassTextField.setPromptText(emptyTextFields);
        } else if (userGroup.isEmpty()) {
            insertWorkgroupTextField.clear();
            insertWorkgroupTextField.setPromptText(emptyTextFields);
        } else {
            User newUser = new User(firstName, lastName, login, pass, email, userGroup);

            DBImp db = new DBImp();
            try {
                db.save(newUser);
            } catch (HibernateException e) {
                e.getCause();
                e.getMessage();
            }
            userTableView.getItems().add(newUser);
            userTableView.refresh();

            insertFirstnameTextFild.clear();
            insertLastnameTextField.clear();
            insertLoginTextField.clear();
            insertPassTextField.clear();
            insertEmailTextField.clear();
            insertWorkgroupTextField.clear();


            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Information Dialog");
            alert.setHeaderText(null);
            alert.setContentText("Zapisano nowego urzytkownika do bazy.");
            alert.showAndWait();
        }
    }

    private void fillUserTab() {
        userTableView.setEditable(true);

        userTableNameColumn.setCellValueFactory(new PropertyValueFactory<>("firstName"));
        userTableNameColumn.setCellFactory(TextFieldTableCell.forTableColumn());
        userTableNameColumn.setOnEditCommit(event -> {
            event.getTableView().getItems().get(
                    event.getTablePosition().getRow()).setFirstName(event.getNewValue());
            dbUpdateUserT(event);
        });

        userTableLastnameColumn.setCellValueFactory(new PropertyValueFactory<>("lastName"));
        userTableLastnameColumn.setCellFactory(TextFieldTableCell.forTableColumn());
        userTableLastnameColumn.setOnEditCommit(event -> {
            event.getTableView().getItems().get(
                    event.getTablePosition().getRow()).setLastName(event.getNewValue());
            dbUpdateUserT(event);
        });

        userTableLoginColumn.setCellValueFactory(new PropertyValueFactory<>("userLogin"));
        userTableLoginColumn.setCellFactory(TextFieldTableCell.forTableColumn());
        userTableLoginColumn.setOnEditCommit(event -> {
            event.getTableView().getItems().get(
                    event.getTablePosition().getRow()).setUserLogin(event.getNewValue());
            dbUpdateUserT(event);
        });

        userTablePasswordColumn.setCellValueFactory(new PropertyValueFactory<>("userPassword"));
        userTablePasswordColumn.setCellFactory(TextFieldTableCell.forTableColumn());
        userTablePasswordColumn.setOnEditCommit(event -> {
            event.getTableView().getItems().get(
                    event.getTablePosition().getRow()).setUserPassword(event.getNewValue());
            dbUpdateUserT(event);
        });

        userTableEmailColumn.setCellValueFactory(new PropertyValueFactory<>("userEmail"));
        userTableEmailColumn.setCellFactory(TextFieldTableCell.forTableColumn());
        userTableEmailColumn.setOnEditCommit(event -> {
            event.getTableView().getItems().get(
                    event.getTablePosition().getRow()).setUserEmail(event.getNewValue());
            dbUpdateUserT(event);
        });

        userTableGroupColumn.setCellValueFactory(new PropertyValueFactory<>("userGroup"));
        userTableGroupColumn.setCellFactory(TextFieldTableCell.forTableColumn());
        userTableGroupColumn.setOnEditCommit(event -> {
            event.getTableView().getItems().get(
                    event.getTablePosition().getRow()).setUserGroup(event.getNewValue());
            dbUpdateUserT(event);
        });

        userTableOptions.setCellValueFactory(new PropertyValueFactory<>("DUMMY"));
        Callback<TableColumn<User, String>, TableCell<User, String>> cellFactory
                = //
                new Callback<TableColumn<User, String>, TableCell<User, String>>() {
                    @Override
                    public TableCell call(final TableColumn<User, String> param) {
                        return new TableCell<User, String>() {

                            final Button btn = new Button("Usuń");

                            @Override
                            public void updateItem(String item, boolean empty) {
                                super.updateItem(item, empty);
                                if (empty) {
                                    setGraphic(null);
                                    setText(null);
                                } else {
                                    btn.setOnAction(event -> {
                                        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                                        alert.setTitle("Zatwierdzenie zmian");
                                        alert.setHeaderText("Przy wybraniu OK nastąpi usunięcie urzytkownika");
                                        alert.setContentText("Kontynuować?");

                                        Optional<ButtonType> result = alert.showAndWait();
                                        if (result.get() == ButtonType.OK) {
                                            User person = getTableView().getItems().get(getIndex());
                                            UserDBImplementation db = new UserDBImplementation();
                                            db.deleteUser(person);
                                            userTableView.getItems().clear();
                                            userTableView.getItems().addAll(getMyUsersFromDB());
                                        } else {
                                            alert.close();
                                        }
                                    });
                                    setGraphic(btn);
                                    setText(null);
                                }
                            }
                        };
                    }
                };
        userTableOptions.setCellFactory(cellFactory);
        userTableView.getItems().addAll(Objects.requireNonNull(getMyUsersFromDB()));
    }

    private void dbUpdateUserT(TableColumn.CellEditEvent<User, String> event) {
        DBImp db = new DBImp();
        db.update(event.getRowValue());
    }

    private List<User> getMyUsersFromDB() {
        UserDBImplementation db = new UserDBImplementation();
        return new ArrayList<>(db.getAllUsers());
    }
}
