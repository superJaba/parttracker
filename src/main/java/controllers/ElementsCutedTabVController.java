package controllers;

import DAO.DBImp;
import DAO.ElementDBImplementation;
import DAO.UserInput;
import controllers.enums.MachinesNames;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.layout.AnchorPane;
import javafx.util.Callback;
import javafx.util.StringConverter;
import javafx.util.converter.IntegerStringConverter;
import model.ElementCut;

import java.net.URL;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class ElementsCutedTabVController implements Initializable {

    //kontrolki Elements TabView
    public AnchorPane elementsCutedTabID;
    @FXML    private TableView<ElementCut> elementTable;
    @FXML    private TableColumn<ElementCut, String> elementNumber;
    @FXML    private TableColumn<ElementCut, Integer> elementAmount;
    @FXML    private TableColumn<ElementCut, String> elementDateCuted;
    @FXML    private TableColumn<ElementCut, String> elementDesc;
    @FXML    private TableColumn<ElementCut, String> elementOptions;
    @FXML    private TableColumn<ElementCut, String> maschineElemetsTable;
    @FXML    private Button buttonRefreshElements;
    @FXML    private TextField searchForCutedElement;
    @FXML    private Button dodajNowyElement;
    @FXML    private TextField nowyElementName;
    @FXML    private TextField nowyElementIlosc;
    @FXML    private TextField nowyElementOpis;
    @FXML    private ChoiceBox<MachinesNames> elementTabChoiceBox;

    private final ObservableList<ElementCut> elementCutObservableList = FXCollections.observableArrayList();

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        fillElementTab(); //fabryka wypelniajaca tabelę wycietych elementow
        controlsMechanicForCuttedElementsTab();
        update();
    }

    private void update() {
        final ScheduledExecutorService service1 = Executors.newScheduledThreadPool(1);
        service1.scheduleAtFixedRate(this::getMeCuteddElements, 605, 605, TimeUnit.SECONDS);
    }

    private void fillElementTab() {

        elementNumber.setCellValueFactory(new PropertyValueFactory<>("partNumber"));
        elementNumber.setCellFactory(TextFieldTableCell.forTableColumn());
        elementNumber.setOnEditCommit(event -> {
            event.getTableView().getItems().get(
                    event.getTablePosition().getRow()).setPartNumber(event.getNewValue());
            dbUpdateElementT(event);
        });

        elementAmount.setCellValueFactory(new PropertyValueFactory<>("partAmount"));
        StringConverter<Integer> stringConverter = new IntegerStringConverter();
        elementAmount.setCellFactory(TextFieldTableCell.forTableColumn(stringConverter));
        elementAmount.setOnEditCommit(event -> {
            event.getTableView().getItems().get(
                    event.getTablePosition().getRow()).setPartAmount(event.getNewValue());
            DBImp db = new DBImp();
            db.update(event.getRowValue());
        });

        elementDateCuted.setCellValueFactory(new PropertyValueFactory<>("dateOfCut"));
        elementDateCuted.setCellFactory(TextFieldTableCell.forTableColumn());
        elementDateCuted.setOnEditCommit(event -> {
            event.getTableView().getItems().get(
                    event.getTablePosition().getRow()).setDateOfCut(event.getNewValue());
            dbUpdateElementT(event);
        });

        elementDesc.setCellValueFactory(new PropertyValueFactory<>("description"));
        elementDesc.setCellFactory(TextFieldTableCell.forTableColumn());
        elementDesc.setOnEditCommit(event -> {
            event.getTableView().getItems().get(
                    event.getTablePosition().getRow()).setDescription(event.getNewValue());
            dbUpdateElementT(event);
        });

        maschineElemetsTable.setCellValueFactory(new PropertyValueFactory<>("machine"));
        maschineElemetsTable.setCellFactory(TextFieldTableCell.forTableColumn());
        maschineElemetsTable.setOnEditCommit(event -> {
            event.getTableView().getItems().get(
                    event.getTablePosition().getRow()).setMachine(event.getNewValue());
            dbUpdateElementT(event);
        });

        elementOptions.setCellValueFactory(new PropertyValueFactory<>("NONEED"));
        Callback<TableColumn<ElementCut, String>, TableCell<ElementCut, String>> cellFactoryEle
                = new Callback<TableColumn<ElementCut, String>, TableCell<ElementCut, String>>() {
            @Override
            public TableCell call(final TableColumn<ElementCut, String> param) {
                return new TableCell<ElementCut, String>() {

                    final Button btn1 = new Button("Usuń");

                    @Override
                    public void updateItem(String item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty) {
                            setGraphic(null);
                            setText(null);
                        } else {
                            btn1.setOnAction(event -> {
                                Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                                alert.setTitle("Zatwierdzenie zmian");
                                alert.setHeaderText("Przy wybraniu OK nastąpi usunięcie elementu");
                                alert.setContentText("Kontynułować?");

                                Optional<ButtonType> result = alert.showAndWait();
                                if (result.get() == ButtonType.OK) {
                                    ElementCut element = getTableView().getItems().get(getIndex());
                                    ElementDBImplementation db = new ElementDBImplementation();
                                    db.deleteElement(element);
                                    elementTable.getItems().clear();
                                    elementTable.getItems().addAll(getMeCuteddElements());
                                } else {
                                    alert.close();
                                }
                            });
                            setGraphic(btn1);
                            setText(null);
                        }
                    }
                };
            }
        };
        elementOptions.setCellFactory(cellFactoryEle);
        elementTable.getItems().addAll(getMeCuteddElements());
    }

    private void dbUpdateElementT(TableColumn.CellEditEvent<ElementCut, String> event) {
        DBImp db = new DBImp();
        db.update(event.getRowValue());
    }

    private List<ElementCut> getMeCuteddElements() {
        ElementDBImplementation elementDB = new ElementDBImplementation();
        elementCutObservableList.clear();
        elementCutObservableList.addAll(elementDB.getAllElements());
        return elementCutObservableList;
    }

    //kasowanie zawartosci pol tekstowych dla nowego elementu w ElementsTabView
    private void clearElementsTabTextFields() {
        nowyElementName.clear();
        nowyElementIlosc.clear();
        nowyElementOpis.clear();
    }

    private void controlsMechanicForCuttedElementsTab() {
        elementTabChoiceBox.getItems().setAll(MachinesNames.values());
        dodajNowyElement.setOnMouseClicked(event -> {
            ElementCut nowyElement = new ElementCut();
            nowyElement.setPartNumber(nowyElementName.getText());
            String toInteger = nowyElementIlosc.getText();
            nowyElement.setPartAmount(Integer.valueOf(toInteger));
            UserInput ui = new UserInput();
            ui.setTimeOfCut(nowyElement);
            nowyElement.setDescription(nowyElementOpis.getText());
            nowyElement.setMachine(elementTabChoiceBox.getValue().toString());
            DBImp db = new DBImp();
            db.save(nowyElement);
            elementTable.getItems().add(nowyElement);
            elementTable.refresh();
            clearElementsTabTextFields();
        });

        buttonRefreshElements.setTooltip(new Tooltip("Wczytuje na nowo \n wyciete elementy."));
        searchForCutedElement.setTooltip(new Tooltip("Wyszukuje podaną nazwę \n w zbiorze wyciętych elementów."));

        //wyszukanie konkretnego wycietego elementu
        searchForCutedElement.textProperty().addListener(((observable, oldValue, newValue) -> {
            elementTable.getItems().clear();
            for (ElementCut elementCutList : elementCutObservableList) {
                if (elementCutList.getPartNumber().contains(observable.getValue())) {
                    elementTable.getItems().addAll(elementCutList);
                    elementTable.refresh();
                }
            }
        }));

        //przeładowanie ElementsTab nowymi danymi z DB
        buttonRefreshElements.setOnAction(event -> {
            searchForCutedElement.clear();
            elementTable.getItems().clear();
            elementTable.getItems().addAll(getMeCuteddElements());
            elementTable.refresh();
        });
    }
}
