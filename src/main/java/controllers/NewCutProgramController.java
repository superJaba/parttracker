package controllers;

import DAO.DBImp;
import controllers.enums.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javafx.util.StringConverter;
import javafx.util.converter.IntegerStringConverter;
import model.CutProgramsDB;
import model.ElementCutForProgramsDB;
import org.hibernate.exception.ConstraintViolationException;
import validation.*;

import java.net.URL;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import java.util.function.Consumer;
import java.util.logging.Logger;

public class NewCutProgramController implements Initializable {

    private ObservableList<ElementCutForProgramsDB> elementObservableList = FXCollections.observableArrayList();

    @FXML
    protected ChoiceBox<MachinesNames> machinesNamesChoiceBox;
    @FXML
    private TableView<ElementCutForProgramsDB> newElementsTable;
    @FXML
    private Button confirmationButton;
    @FXML
    private Button cancelButton;
    @FXML
    private AnchorPane newProgramLayout;
    @FXML
    private TableColumn<ElementCutForProgramsDB, Integer> nowyElementIlosc;
    @FXML
    private TableColumn<ElementCutForProgramsDB, String> nowyElementNazwa;
    @FXML
    private DatePicker datePicker;
    @FXML
    private TextField dimX;
    @FXML
    private TextField dimY;
    @FXML
    private TextField nazwaNowegoProgramu;
    @FXML
    private TextField iloscElementow;
    @FXML
    private ChoiceBox<PlateThickness> materialThicknes;
    @FXML
    private ChoiceBox<PlateMaterials> plateMaterial;
    @FXML
    private ChoiceBox<String> plateType;
    @FXML
    private Label duplikatProgramuLabel;

    private CutProgramsDB cutProgramsDB = new CutProgramsDB();
    private List<CutProgramsDB> list = new ArrayList<>();
    private static final Logger logger = Logger.getAnonymousLogger();
    private Consumer<CutProgramsDB> onComplete;


    private TableView<CutProgramsDB> cutProgramsDBTableView = null;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        try {
            startMethod();
        } catch (Exception e) {
            e.printStackTrace();
        }
        tableDataInit();
        materialThicknes.setOnMouseClicked(event -> loadPlateTypes());


        nazwaNowegoProgramu.textProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue.length() >= 3) {
                for (CutProgramsDB cp : list) {
                    if (cp.getProgramName().contains(newValue)) {
                        duplikatProgramuLabel.setText("Program o podanej nazwie już istnieje");
                        duplikatProgramuLabel.setVisible(true);
                        confirmationButton.setVisible(false);
                        break;
                    } else {
                        duplikatProgramuLabel.setVisible(false);
                        confirmationButton.setVisible(true);
                    }
                }
            }
        });
    }


    @FXML
    void onActionCancelButton(ActionEvent event) {
        windowClose(cancelButton);
    }


    private void startMethod() throws Exception {
        machinesNamesChoiceBox.getItems().addAll(MachinesNames.values());
        materialThicknes.getItems().addAll(PlateThickness.values());
        plateMaterial.getItems().addAll(PlateMaterials.values());
        datePicker.showWeekNumbersProperty();
        String pattern = "yyyy-MM-dd";
        datePicker.setPromptText(pattern.toLowerCase());
        datePicker.setConverter(new StringConverter<LocalDate>() {
            DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(pattern);

            @Override
            public String toString(LocalDate object) {
                if (object != null) {
                    return dateTimeFormatter.format(object);
                } else return "";
            }

            @Override
            public LocalDate fromString(String string) {
                if (string != null && !string.isEmpty()) {
                    return LocalDate.parse(string, dateTimeFormatter);
                } else {
                    return null;
                }
            }
        });
    }

    @FXML
    void onActionConfirmButton(ActionEvent event) {
        List<ElementCutForProgramsDB> elementsList = new ArrayList<>();
        cutProgramsDB.setProgramName(nazwaNowegoProgramu.getText());

        String sheetDim = dimX.getText() + " x " + dimY.getText();

        cutProgramsDB.setSheetDimensions(sheetDim);
        String stringFromChoiceBox = materialThicknes.getValue().toString() +
                " " + plateMaterial.getValue().toString() + " " + plateType.getValue();

        cutProgramsDB.setSheetThicknes(stringFromChoiceBox);
        cutProgramsDB.setMaschine(String.valueOf(machinesNamesChoiceBox.getValue()));
        LocalDate localDate = datePicker.getValue();
        Instant instant = localDate.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant();
        cutProgramsDB.setDateOfGeneration(Date.from(instant));
        for (ElementCutForProgramsDB elementCutForProgramsDB : elementObservableList) {
            elementCutForProgramsDB.setMachine(cutProgramsDB.getMaschine());
            elementCutForProgramsDB.setCutProgramsDB(cutProgramsDB);
            elementsList.add(elementCutForProgramsDB);
        }
        cutProgramsDB.setElementCutList(elementsList);
//        onComplete.accept(cutProgramsDB);
        DBImp db = new DBImp();
        try {
            db.save(cutProgramsDB);
        } catch (ConstraintViolationException e) {
            logger.info(e.getMessage());
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setHeaderText("Podana nazwa programu już istnieje.");
            alert.setContentText(logger.getClass().toString());
        }
        windowClose(confirmationButton);
    }

    private static void windowClose(Button button) {
        Stage stage = (Stage) button.getScene().getWindow();
        stage.close();
    }

    private void tableDataInit() {
        machinesNamesChoiceBox.setOnMouseClicked(event -> {
            InputCheck inputCheck = new InputCheck();
            int iloscEle = inputCheck.isNumber(iloscElementow.getText());
            if (iloscEle == -1) {
                duplikatProgramuLabel.setText("Niepoprawne dane w polu ilości elementów w programie.");
                duplikatProgramuLabel.setVisible(true);
            } else {
                duplikatProgramuLabel.setVisible(false);
                elementObservableList.clear();
                newElementsTable.getItems().clear();
                newElementsTable.refresh();
                for (int i = 1; i < iloscEle + 1; i++) {
                    ElementCutForProgramsDB element = new ElementCutForProgramsDB();
                    element.setPartNumber("Wpisz nazwę " + i + " elementu.");
                    element.setPartAmount(0);
                    elementObservableList.add(element);
                    newElementsTable.getItems().add(element);
                    newElementsTable.refresh();
                }
            }
        });
        tableFabric();
    }

    private void tableFabric() {

        nowyElementNazwa.setCellValueFactory(new PropertyValueFactory<>("partNumber"));
        nowyElementNazwa.setCellFactory(TextFieldTableCell.forTableColumn());
        nowyElementNazwa.setOnEditCommit(event -> {
            event.getTableView().getItems().get(
                    event.getTablePosition().getRow()).setPartNumber(event.getNewValue());
        });

        nowyElementIlosc.setCellValueFactory(new PropertyValueFactory<>("partAmount"));
        StringConverter<Integer> stringConverter = new IntegerStringConverter();
        nowyElementIlosc.setCellFactory(TextFieldTableCell.forTableColumn(stringConverter));
        nowyElementIlosc.setOnEditCommit(event -> {
            event.getTableView().getItems().get(
                    event.getTablePosition().getRow()).setPartAmount(event.getNewValue());
        });
    }

    public void setList(List<CutProgramsDB> list) {
        this.list = list;
    }

    private void loadPlateTypes() {
        String materialType = String.valueOf(plateMaterial.getValue());
        plateType.getItems().clear();
        String[] comboName;
        PlateType[] plates = null;
        if (materialType != null) {
            switch (materialType) {
                case "ALUMINIUM":
                    plates = AlmgPlateType.values();
                    comboName = new String[plates.length];
                    break;
                case "INOX":
                    plates = InoxPlateType.values();
                    comboName = new String[plates.length];
                    break;
                default:
                    plates = SteelPlateType.values();
                    comboName = new String[plates.length];
                    break;
            }
            for (int i = 0; i < plates.length; i++) {
                comboName[i] = plates[i].toString();
            }
            plateType.getItems().addAll(comboName);
        }
    }

    public void initData(ObservableList<CutProgramsDB> observableList, Consumer<CutProgramsDB> onComplete) {
        list.addAll(observableList);
        this.onComplete = onComplete;
    }
}