package validation;

import DAO.DBImp;
import DAO.UserDBImplementation;
import model.User;

import java.util.ArrayList;
import java.util.List;

public class LoginUserControl {


    private List<User> userList = new ArrayList<>();
    private User userLogged;


    private void mLoginFromDB() {
        UserDBImplementation db = new UserDBImplementation();
        userList.addAll(db.getAllUsers());

        if (userList.isEmpty()) {
            User makeUser = new User();
            makeUser.setUserLogin("root");
            makeUser.setUserPassword("test");
            makeUser.setUserGroup("superAdmin");
            userList.add(makeUser);
            DBImp dbImp = new DBImp();
            dbImp.save(makeUser);
        }
    }

    public User mCheckUser(User user) {
        mLoginFromDB();

        for (User anUserList : userList) {
            if (anUserList.getUserLogin().equals(user.getUserLogin()) && anUserList.getUserPassword().equals(user.getUserPassword())) {
                userLogged = new User();
                userLogged.setUserLogin(anUserList.getUserLogin());
                userLogged.setUserPassword(anUserList.getUserPassword());
                userLogged.setUserGroup(anUserList.getUserGroup());
                return userLogged;
            }
        }
        return null;
    }

    public User getUserLogged() {
        return userLogged;
    }
}
