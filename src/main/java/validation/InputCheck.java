package validation;

public class InputCheck {


    public int isNumber(String text) {
        int temp = 0;
        try {
            temp = Integer.valueOf(text);
        } catch (NumberFormatException e) {
           return -1;
        }
        return temp;
    }
}
