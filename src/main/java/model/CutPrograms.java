package model;

import com.google.gson.annotations.SerializedName;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * Created by Karamba on 2017-05-16
 */

@Getter
@Setter
@AllArgsConstructor
public class CutPrograms {

    @SerializedName("PROGRAM NAME")
    protected String programName;
    @SerializedName("DATE OF GENERATION")
    protected String dateOfGeneration;
    @SerializedName("SHEET DIMENSIONS")
    protected String sheetDimensions;
    @SerializedName("SHEET THICKNESS")
    protected String sheetThicknes;
    @SerializedName("LIST OF ELEMENTS")
    protected List<ElementCutOldVer> elementCutted;

    public CutPrograms() {
    }

    @Override
    public String toString() {
        return "Nazwa programu = " + programName +
                ", Data wygenerowania = " + dateOfGeneration +
                ", Wymiary arkusza = " + sheetDimensions +
                ", Grubość arkusza = " + sheetThicknes +
                ", \n\tElementy w programie :\n" + toStringBis(elementCutted);
    }

    private String toStringBis(List<ElementCutOldVer> elementCutted) {
        StringBuilder opis = new StringBuilder();
        for (ElementCutOldVer elementCut : elementCutted) {
            if (elementCut.getDescription().isEmpty()) {
                opis.append("\t\tnumer elementu = ")
                        .append(elementCut.getPartNumber())
                        .append(" | ilość = " + elementCut.getPartAmount())
                        .append("\n");
            } else {
                opis.append("\t\tnumer elementu = ")
                        .append(elementCut.getPartNumber())
                        .append(" | ilość = " + elementCut.getPartAmount())
                        .append(" | opis = " + elementCut.getDescription())
                        .append("\n");
            }
        }
        return opis.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CutPrograms that = (CutPrograms) o;

        if (!getProgramName().equals(that.getProgramName())) return false;
        if (!getElementCutted().equals(that.getElementCutted())) return false;
        if (getDateOfGeneration() != null ? !getDateOfGeneration().equals(that.getDateOfGeneration()) : that.getDateOfGeneration() != null)
            return false;
        if (!getSheetDimensions().equals(that.getSheetDimensions())) return false;
        return getSheetThicknes().equals(that.getSheetThicknes());
    }

    @Override
    public int hashCode() {
        int result = getProgramName().hashCode();
        result = 31 * result + getElementCutted().hashCode();
        result = 31 * result + (getDateOfGeneration() != null ? getDateOfGeneration().hashCode() : 0);
        result = 31 * result + getSheetDimensions().hashCode();
        result = 31 * result + getSheetThicknes().hashCode();
        return result;
    }
}
