package model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;

@Getter
@Setter
@ToString
@Entity
@org.hibernate.annotations.DynamicUpdate
@Table(name = "plate")
public class Plate {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", unique = true, nullable = false)
    private Integer plateID;
    @Column(name = "substance")
    private String substance; //material
    @Column(name = "thickness")
    private Double thickness; //grubosc
    @Column(name = "type")
    private String type; //gatunek
    @Column(name = "dimX")
    private Integer dimX; //pierwszy wymiar
    @Column(name = "dimY")
    private Integer dimY; //drugi wymiar
    @Column(name = "amount")
    private Integer amount; //ilosc
    @Column(name = "reservation")
    private Integer reservation;
    @Column(name = "description")
    private String description; //miejsce skladowania

}
