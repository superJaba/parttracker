package model;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class ControlCart {

    private String orderNo; //nr zamowienia
    private String completionDate; // termin wykonania
    private String drawingNumber; //Nr rysunku detalu
    private String customer; //klient
    private String toDetail; //do detalu
    private String steelGrade; //gatunek stali
    private String materialThicknes; //grubosc materialu
    private int orderedQuantity; //ilosc detali
    private String criticalPoints; //
    private boolean isSerialProduction; //czy prod seryjna
    private List<String> workPosition; //lista dzaiłów
    private String dateOfDone; //data zakonczenia na dziale
    private int goodPiecesQuantity;
    private String makerName;

    public ControlCart() {
    }

    public ControlCart(String orderNo, String completionDate, String drawingNumber, String customer,
                       String toDetail, String steelGrade, String materialThicknes, int orderedQuantity, String criticalPoints,
                       boolean isSerialProduction, List<String> workPosition, String dateOfDone,
                       int goodPiecesQuantity, String makerName) {
        this.orderNo = orderNo;
        this.completionDate = completionDate;
        this.drawingNumber = drawingNumber;
        this.customer = customer;
        this.toDetail = toDetail;
        this.steelGrade = steelGrade;
        this.orderedQuantity = orderedQuantity;
        this.criticalPoints = criticalPoints;
        this.isSerialProduction = isSerialProduction;
        this.workPosition = workPosition;
        this.dateOfDone = dateOfDone;
        this.goodPiecesQuantity = goodPiecesQuantity;
        this.makerName = makerName;
        this.materialThicknes = materialThicknes;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ControlCart that = (ControlCart) o;

        if (orderedQuantity != that.orderedQuantity) return false;
        if (isSerialProduction != that.isSerialProduction) return false;
        if (goodPiecesQuantity != that.goodPiecesQuantity) return false;
        if (orderNo != null ? !orderNo.equals(that.orderNo) : that.orderNo != null) return false;
        if (completionDate != null ? !completionDate.equals(that.completionDate) : that.completionDate != null)
            return false;
        if (drawingNumber != null ? !drawingNumber.equals(that.drawingNumber) : that.drawingNumber != null)
            return false;
        if (customer != null ? !customer.equals(that.customer) : that.customer != null) return false;
        if (toDetail != null ? !toDetail.equals(that.toDetail) : that.toDetail != null) return false;
        if (steelGrade != null ? !steelGrade.equals(that.steelGrade) : that.steelGrade != null) return false;
        if (materialThicknes != null ? !materialThicknes.equals(that.materialThicknes) : that.materialThicknes != null)
            return false;
        if (criticalPoints != null ? !criticalPoints.equals(that.criticalPoints) : that.criticalPoints != null)
            return false;
        if (workPosition != null ? !workPosition.equals(that.workPosition) : that.workPosition != null) return false;
        if (dateOfDone != null ? !dateOfDone.equals(that.dateOfDone) : that.dateOfDone != null) return false;
        return makerName != null ? makerName.equals(that.makerName) : that.makerName == null;
    }

    @Override
    public int hashCode() {
        int result = orderNo != null ? orderNo.hashCode() : 0;
        result = 31 * result + (completionDate != null ? completionDate.hashCode() : 0);
        result = 31 * result + (drawingNumber != null ? drawingNumber.hashCode() : 0);
        result = 31 * result + (customer != null ? customer.hashCode() : 0);
        result = 31 * result + (toDetail != null ? toDetail.hashCode() : 0);
        result = 31 * result + (steelGrade != null ? steelGrade.hashCode() : 0);
        result = 31 * result + (materialThicknes != null ? materialThicknes.hashCode() : 0);
        result = 31 * result + orderedQuantity;
        result = 31 * result + (criticalPoints != null ? criticalPoints.hashCode() : 0);
        result = 31 * result + (isSerialProduction ? 1 : 0);
        result = 31 * result + (workPosition != null ? workPosition.hashCode() : 0);
        result = 31 * result + (dateOfDone != null ? dateOfDone.hashCode() : 0);
        result = 31 * result + goodPiecesQuantity;
        result = 31 * result + (makerName != null ? makerName.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Zlecenie{" +
                "numer='" + orderNo + '\'' +
                ", termin wykonania='" + completionDate + '\'' +
                ", numer rysunku='" + drawingNumber + '\'' +
                ", klient='" + customer + '\'' +
                ", do detalu='" + toDetail + '\'' +
                ", material='" + steelGrade + '\'' +
                ", grubosc materialu='" + materialThicknes + '\'' +
                ", zamowiona ilosc=" + orderedQuantity +
                ", punkty krytyczne='" + criticalPoints + '\'' +
                ", produkcja seryjna=" + isSerialProduction +
                ", maszyna \\ stanowiska=" + workPosition +
                '}';
    }
}
