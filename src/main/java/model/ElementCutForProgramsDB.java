package model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.io.Serializable;

@Getter
@Setter
@ToString
@Entity
@DynamicUpdate
@Table(name = "elementsDB")
public class ElementCutForProgramsDB implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "elementID", unique = true, nullable = false)
    private long id;

    @Column(name = "partNumber")
    protected String partNumber;

    @Column(name = "amount")
    protected Integer partAmount;

    @Column( name = "machine")
    private String machine;

    @ManyToOne( fetch = FetchType.LAZY)
    @JoinColumn(name = "FGkey", nullable = false)
    private CutProgramsDB cutProgramsDB;

    public ElementCutForProgramsDB() {
    }

    public ElementCutForProgramsDB(String partNumber, Integer partAmount, String machine) {
        this.partNumber = partNumber;
        this.partAmount = partAmount;
        this.machine = machine;
    }
}
