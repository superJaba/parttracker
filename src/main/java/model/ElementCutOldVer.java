package model;

import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Objects;

@Getter
@Setter
public class ElementCutOldVer implements Serializable {

    @SerializedName("part number")
    protected String partNumber;
    @SerializedName("amount")
    protected Integer partAmount;
    @SerializedName("cut date")
    protected String dateOfCut;
    @SerializedName("opis")
    protected String description;

    public ElementCutOldVer(String partNumber, Integer partAmount, String dateOfCut, String description) {
        this.partNumber = partNumber;
        this.partAmount = partAmount;
        this.dateOfCut = dateOfCut;
        this.description = description;
    }

    public ElementCutOldVer() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ElementCutOldVer)) return false;
        ElementCutOldVer that = (ElementCutOldVer) o;
        return Objects.equals(partNumber, that.partNumber) &&
                Objects.equals(partAmount, that.partAmount) &&
                Objects.equals(dateOfCut, that.dateOfCut) &&
                Objects.equals(description, that.description);
    }

    @Override
    public int hashCode() {

        return Objects.hash(partNumber, partAmount, dateOfCut, description);
    }
}
