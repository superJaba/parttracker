package model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * Created by Karamba on 2017-05-16
 */
@Getter
@Setter
@Entity
@DynamicUpdate
@Table(name = "ELEMENTS_CUTED")
public class ElementCut implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", unique = true, nullable = false)
    private long id;

    @Column(name = "PART_NUMBER")
    protected String partNumber;
    @Column(name = "AMOUNT")
    protected Integer partAmount;
    @Column(name = "CUT_DATE")
    protected String dateOfCut;
    @Column(name = "DESCRIPTION")
    protected String description;
    @Column( name = "machine")
    private String machine;


    public ElementCut(String partNumber, Integer partAmount, String description) {
        this.partNumber = partNumber;
        this.partAmount = partAmount;
        this.description = description;

    }

    public ElementCut(String partNumber, Integer partAmount) {
        this.partNumber = partNumber;
        this.partAmount = partAmount;
    }

    public ElementCut(String partNumber, Integer partAmount, String dateOfCut, String description) {
        this.partNumber = partNumber;
        this.partAmount = partAmount;
        this.dateOfCut = dateOfCut;
        this.description = description;
    }

    public ElementCut() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ElementCut)) return false;
        ElementCut that = (ElementCut) o;
        return id == that.id &&
                Objects.equals(partNumber, that.partNumber) &&
                Objects.equals(partAmount, that.partAmount) &&
                Objects.equals(dateOfCut, that.dateOfCut) &&
                Objects.equals(description, that.description) &&
                Objects.equals(machine, that.machine);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, partNumber, partAmount, dateOfCut, description, machine);
    }

    @Override
    public String toString() {
        return  "Numer elementu = " + partNumber  +
                ", ilosc = " + partAmount  +
                ", data wyciecia = " + dateOfCut  +
                ", opis = " + description +
                ", maszyna = " + machine;
    }
}
