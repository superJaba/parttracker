package model;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class Stanowisko {
    private String stanowisko;
    private Date dataZakonczeniaObrobki;

    public Stanowisko() {
    }
}
