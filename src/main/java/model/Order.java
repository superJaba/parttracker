package model;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.List;

@Setter
@Getter
public class Order {

    private String orderName;
    private Date deadline;
    private Date terminWystawienia;
    private String nrRysunkuDetalu;
    private String drawningPath;
    private String customer;
    private String wholePartNumber;
    private String material;
    private  int partAmountNeeded;
    private int partAmountProduced;
    private String criticalPoints;
    private boolean productionType; //nowy detal lub produkcja seryjna
    private List<Stanowisko> listaStanowisk;
    private String operator;

    public Order() {
    }

}
