package model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;

@Getter
@Setter
@ToString
@Entity
@org.hibernate.annotations.DynamicUpdate
@Table(name = "User")
public class User implements Serializable {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", unique = true, nullable = false)
    private Integer userID;
    @Column(name = "FIRST_NAME", nullable = false)
    private String firstName;
    @Column(name = "LAST_NAME", nullable = false)
    private String lastName;
    @Column(name = "LOGIN", unique = true, nullable = false)
    private String userLogin;
    @Column(name = "PASSWORD", nullable = false)
    private String userPassword;
    @Column(name = "EMAIL", unique = true)
    private String userEmail;
    @Column(name = "USER_GROUP", nullable = false)
    private String userGroup;

    public User() {
    }

    public User(String userLogin, String userPassword) {
        this.userLogin = userLogin;
        this.userPassword = userPassword;
    }

    public User(String firstName, String lastName, String userLogin, String userPassword, String userEmail, String userGroup) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.userLogin = userLogin;
        this.userPassword = userPassword;
        this.userEmail = userEmail;
        this.userGroup = userGroup;
    }

}
