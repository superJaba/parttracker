package model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@Entity
@DynamicUpdate
@Table(name = "CUT_PROGRAMS")
public class CutProgramsDB implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "programID", unique = true, nullable = false)
    private Integer id;

    @Column(name = "program_name", nullable = false, unique = true)
    protected String programName;

    @Temporal(TemporalType.DATE)
    @Column(name = "generation_date")
    protected Date dateOfGeneration;

    @Column(name = "sheet_dimm")
    protected String sheetDimensions;

    @Column(name = "material_info")
    protected String sheetThicknes;

    @Column(name = "maschine")
    private String maschine;

    @OneToMany( mappedBy = "cutProgramsDB", fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
    protected List<ElementCutForProgramsDB> elementCutList = new ArrayList<>();

    public CutProgramsDB() {
    }
}
