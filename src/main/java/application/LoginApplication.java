package application;

import controllers.*;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;
import javafx.stage.Screen;
import javafx.stage.Stage;
import model.User;
import validation.*;

import java.io.IOException;

public class LoginApplication extends Application {

    private final Text notification = new Text();
    private TextField userNameField;
    private PasswordField passwordField;
    private final String cssPath = "application.css";

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {

        primaryStage.setTitle("Karambix");

        GridPane grid = new GridPane();
        grid.setAlignment(Pos.CENTER);
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(25, 25, 25, 25));

//        sceneTitle = new Text("Witaj");
//        sceneTitle.setId("sceneTitle");
//        grid.add(sceneTitle, 0, 0, 2, 1);

        Label userNameLabel = new Label("Login:");
        grid.add(userNameLabel, 0, 1);

        userNameField = new TextField();
        grid.add(userNameField, 1, 1);

        Label passwordLabel = new Label("Hasło:");
        grid.add(passwordLabel, 0, 2);

        passwordField = new PasswordField();
        grid.add(passwordField, 1, 2);

        Button button = new Button("Zaloguj");
        HBox hBoxPane = new HBox(10);
        hBoxPane.setAlignment(Pos.BOTTOM_RIGHT);
        hBoxPane.getChildren().add(button);
        grid.add(hBoxPane, 1, 4);
//        grid.setGridLinesVisible(true);//pokazuje siatke

        notification.setId("notification");
        grid.add(notification, 1, 5);

        passwordField.setOnKeyPressed(event -> {
            if (event.getCode() == KeyCode.ENTER) {
                newStageAfterUserInput(primaryStage);
            }
        });

        button.setOnAction((ActionEvent event) -> newStageAfterUserInput(primaryStage));

        Scene scene = new Scene(grid, 350, 300);
        primaryStage.setScene(scene);

//        cssPath = String.valueOf(this.getClass().getResource("application.css"));
        scene.getStylesheets().add(cssPath);
        primaryStage.setOnCloseRequest(event -> System.exit(0));
        primaryStage.show();
    }

    private void newStageAfterUserInput(Stage primaryStage) {
        User userToLogin = new User(userNameField.getText(), passwordField.getText());
        LoginUserControl luc = new LoginUserControl();
        User checkUser = luc.mCheckUser(userToLogin);
        if (checkUser != null) {
            notification.setText("Zalogowano");
            try {
                showAdminPanel(primaryStage, checkUser);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            notification.setText("Login lub hasło jest niepoprawne");
        }
    }

    private void showAdminPanel(Stage primaryStage, User user) throws IOException {
        ProgramController programController = new ProgramController();
        programController.setLoggedUser(user);
        programController.setWindowOwner(primaryStage);


        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(this.getClass().getResource("/fxmlFiles/panelAdmina.fxml"));
        loader.setController(programController);
        BorderPane borderPane = loader.load();
        Scene scene = new Scene(borderPane);
        primaryStage.setScene(scene);

        //centralna pozycja na ekranie
        Rectangle2D primScreenBounds = Screen.getPrimary().getVisualBounds();
        primaryStage.setX((primScreenBounds.getWidth() - primaryStage.getWidth()) / 2);
        primaryStage.setY((primScreenBounds.getHeight() - primaryStage.getHeight()) / 2);

        primaryStage.show();
    }


}
