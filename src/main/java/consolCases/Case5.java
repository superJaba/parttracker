package consolCases;

import DAO.MyFileWriter;
import DAO.ReadFromFile;
import DAO.UserInput;
import model.CutPrograms;
import model.ElementCut;

import java.util.ArrayList;
import java.util.List;

class Case5 implements MyIStrings {
    private static UserInput userInput = new UserInput();

    static void mCase5(ReadFromFile readFromFile) {
        System.out.println("Jaki program został wycięty? ");
        String w = userInput.getUserInput();

        List<CutPrograms> temp = new ArrayList<>(readFromFile.mSearchForProgram(w));
        if (temp.isEmpty()) {
            System.out.println(ConsoleMain.NO_SUCH_FILE_ERROR);
        } else {
            if (temp.size() > 1) {
                System.out.println("Doprecyzuj program. Jest więcej podobnych programow w bazie");
                for (int i = 0; i < temp.size(); i++) {
                    System.out.println((i + 1) + ": " + temp.get(i).getProgramName());
                }
                Integer pos = ConsoleMain.mCheckChoice();
                List<CutPrograms> aTemp = new ArrayList<>();
                aTemp.add(temp.get(pos - 1));
                elementPOWNoSheets(aTemp);
            } else {
                elementPOWNoSheets(temp);
            }
        }
    }

    private static void elementPOWNoSheets(List<CutPrograms> temp) {
        System.out.println("Ile blach zostało wyciętych? ");
        Integer iSheets = ConsoleMain.mCheckChoice();
        mCheckPartsFromProgram(iSheets, temp);
    }

    private static void mCheckPartsFromProgram(Integer iSheets, List<CutPrograms> temp) {
        int iListPosition = 0;
        for (CutPrograms aTemp : temp) {
            iListPosition = mCheckPartBeforeWriting(iSheets, iListPosition, aTemp);
        }
    }

    private static int mCheckPartBeforeWriting(Integer iSheets, int iListPosition, CutPrograms aTemp) {
        for (int j = iListPosition; j < aTemp.getElementCutted().size(); j++) {
            ElementCut elementCut = new ElementCut();
            elementCut.setPartNumber(aTemp.getElementCutted().get(j).getPartNumber());
            Integer partAmount = aTemp.getElementCutted().get(j).getPartAmount();
            partAmount = partAmount * iSheets;
            System.out.println("Czy liczba elementów: " + elementCut.getPartNumber() + " --"
                    + partAmount + "-- sie zgadza? t/n");
            String input = userInput.getUserInput();
            if (input.equals("t") || input.isEmpty()) {
                elementCut.setPartAmount(partAmount);
            } else if (input.equals("n")) {
                System.out.println("Ile elementów trafi do odpadu?");
                int garbage = ConsoleMain.mCheckChoice();
                elementCut.setPartAmount(partAmount - garbage);
            }
            System.out.println(MyIStrings.INS_PART_DESCR);
            elementCut.setDescription(userInput.getUserInput());
            userInput.setTimeOfCut(elementCut);
            System.out.println("Sprawdź poprawność danych:");
            System.out.println(elementCut.toString());
            System.out.println("Wszystko się zgadza? \"t\" lub \"n\"");
            String sUserAnswer = userInput.getUserInput();
            if (sUserAnswer.equals("t") || sUserAnswer.isEmpty()) {
                MyFileWriter.writeToFileElement(elementCut);
                System.out.println(WRITED);
                iListPosition = j;
            } else if (sUserAnswer.equals("n")) {
                iListPosition = j;
//                    System.out.println(elementCut.toString());
                System.out.println("--Poprawiasz od " + iListPosition + " elementu!--");
                mCheckPartBeforeWriting(iSheets, iListPosition, aTemp);
            }
        }
        return iListPosition;
    }
}