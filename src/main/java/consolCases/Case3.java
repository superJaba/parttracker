package consolCases;

import DAO.ReadFromFile;

public class Case3 {
    static void mCase3(ReadFromFile readFromFile) {
        System.out.println(ConsoleMain.CASE3_UNDER_MENU);
        int s = ConsoleMain.mCheckChoice();
        if (s == 1) {
            readFromFile.readAllElements();
            System.out.println();
        } else if (s == 2) {
            System.out.println(ConsoleMain.INS_PART_NAME);
            String part = ConsoleMain.userInput.getUserInput();
            readFromFile.searchElement(part);
        } else {
            System.out.println(ConsoleMain.WRONG_OPERATION_CHOICE);
            mCase3(readFromFile);
        }
    }
}