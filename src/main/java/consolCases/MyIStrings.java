package consolCases;

/**
 * Created by Karamba on 2017-09-25
 */
public interface MyIStrings {
    String WRITED = "--Zapisano--\n";
    String WRONG_OPERATION_CHOICE = "--BŁĘDNY WYBÓR OPERACJI!--";
    String CHOSE = "Co chcesz zrobic?";
    String INSERT_No_OF_ELEMENTS = "Ile jest różnych elementów?";
    String INSERT_PROGRAM_NAME = "Podaj nazwe programu: ";
    String INS_SHEET_DIMENSION = "Podaj wymiary arkusza blachy: ";
    String INS_THICKNESS = "Podaj grubosc materiału: ";
    String INS_DATE_OF_PR_CREATION = "Podaj date utworzenia programu: ";

    String INS_No_OF_ELEMENTS = "Podaj ilosc elementow: ";
    String INS_PART_DESCR = "Podaj uwagi lub miejsce odlozenia: ";
    String WTF = "Co Ty mi tu wpisujesz??";
    String CASE4_UNDER_MENU = "1. Wypisz wszystkie programy.\n" +
            "2. Wyszukaj program wg konkretnego elementu.";
    String INS_PART_NEEDED = "Podaj nazwę potrzebnego elementu: ";
    String MENU =
            "--------- MENU -----------\n"
                    + "1. Dodaj wyciety element.\n"
                    + "2. Opcje programów.\n"
                    + "3. Wyszukaj wyciety element.\n"
                    + "4. Przeszukaj programy pod wzgledem elementu.\n"
                    + "5. Załaduj elementy z wpisanych programów. \n"
                    + "6. Dodaj zlecenie produkcyjne.\n"
                    + "0. Koniec programu\n"
                    + "--------------------------";
    String CASE3_UNDER_MENU = "1. Wypisz wszystkie elementy.\n"
            + "2. Wyszukaj konkretny element";
    String INS_PART_NAME = "Podaj numer detalu: ";
    String INS_PART_AMOUNT = "Podaj ilosc elementow: ";
    String INS_PROPER_VALUE = "Podaj poprawną wartość!";
    String NO_SUCH_FILE_ERROR = "Nie ma takiego programu. Sprawdź nazwę i spróbuj jeszcze raz";
    String UPS = "Ups. Chyba nie ma tego, czego szukasz.";
    String SOMETHING_WENT_WRONG = "Coś jednak poszło nie tak";
}
