package consolCases;

import DAO.MyFileWriter;
import DAO.UserInput;
import model.ElementCut;

class Case1 implements MyIStrings {

    private static UserInput userInput = new UserInput();
    private static ElementCut elementCut = new ElementCut();

    static void mCase1() {
        System.out.println(ConsoleMain.INSERT_No_OF_ELEMENTS);
        Integer noOfElements = ConsoleMain.mCheckChoice();
        if (noOfElements == -1) {
            System.out.println(MyIStrings.INS_PROPER_VALUE);
            mCase1();
        } else {
            for (int i = 0; i < noOfElements; i++) {
                elementUserInput(i);
                userInput.setTimeOfCut(elementCut);//automatic
                MyFileWriter.writeToFileElement(elementCut);
                System.out.println(MyIStrings.WRITED);
            }
        }
    }

    private static void elementUserInput(int i) {
        System.out.println("Podaj nazwe " + (i + 1) + " elementu: ");
        elementCut.setPartNumber(userInput.getUserInput());

        System.out.println(MyIStrings.INS_No_OF_ELEMENTS);
        elementCut.setPartAmount(ConsoleMain.mCheckChoice());

        System.out.println(MyIStrings.INS_PART_DESCR);
        elementCut.setDescription(userInput.getUserInput());
    }
}