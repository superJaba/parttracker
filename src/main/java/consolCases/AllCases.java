package consolCases;

import DAO.ReadFromFile;


public class AllCases implements ICase {

    @Override
    public void mCase1() {
        Case1.mCase1();
    }

    @Override
    public void mCase2() {
        Case2.mCase2();
    }

    @Override
    public void mCase3(ReadFromFile readFromFile) {
        Case3.mCase3(readFromFile);
    }

    @Override
    public void mCase4(ReadFromFile readFromFile) {
        Case4.mCase4(readFromFile);
    }

    @Override
    public void mCase5(ReadFromFile readFromFile) {
        Case5.mCase5(readFromFile);
    }

    @Override
    public void mCase6() {
        Case6.mCase6();
    }
}
