package consolCases;

import DAO.AntiDoublePrograms;
import DAO.MyFileWriter;
import DAO.ReadFromFile;
import DAO.UserInput;
import com.google.gson.Gson;
import model.CutPrograms;
import model.ElementCutOldVer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

class Case2 implements MyIStrings {

    private static CutPrograms cutPrograms;
    private static List<ElementCutOldVer> listOfElements;
    private static UserInput userInput = new UserInput();

    static void mCase2() {
        System.out.println("1. Dodaj program do bazy. \n" +
                "2. Wyszukaj program w bazie.\n" +
                "3. Edycja programu.\n" +
                "4. Usuń program z bazy.\n" +
                "0. Powrót.");

        Integer intFirsChoice = mCheckChoice();
        if (intFirsChoice < 0 || intFirsChoice > 4) {
            System.out.println("Błędny wybór operacji.");
        }
        switch (intFirsChoice) {
            case 1:
                cutPrograms = new CutPrograms();
                listOfElements = new ArrayList<>();
                System.out.println(MyIStrings.INSERT_PROGRAM_NAME);
                cutPrograms.setProgramName(userInput.getUserInput());
                System.out.println(MyIStrings.INS_SHEET_DIMENSION);
                cutPrograms.setSheetDimensions(userInput.getUserInput());
                System.out.println(MyIStrings.INS_THICKNESS);
                cutPrograms.setSheetThicknes(userInput.getUserInput());
                System.out.println(MyIStrings.INS_DATE_OF_PR_CREATION);
                cutPrograms.setDateOfGeneration(userInput.getUserInput());
                System.out.println(MyIStrings.INSERT_No_OF_ELEMENTS);
                ConsoleMain.noOfElements = mCheckChoice();

                for (int i = 0; i < ConsoleMain.noOfElements; i++) {
                    ElementCutOldVer e = new ElementCutOldVer();
                    int i1 = 1;
                    System.out.println("Podaj nazwe " + (i + i1) + " elementu: ");
                    e.setPartNumber(userInput.getUserInput());
                    System.out.println(MyIStrings.INS_PART_AMOUNT);
                    e.setPartAmount(mCheckChoice());
//            System.out.println(INS_PART_DESCR);
                    e.setDescription("");
                    listOfElements.add(e);
                }
                cutPrograms.setElementCutted(listOfElements);
                System.out.println("Sprawdź poprawność danych\t \"t\" lub ENTER\\\"n\"");
                System.out.println("---------------------------------------------------");
                System.out.println(cutPrograms);
                System.out.println("---------------------------------------------------");
                System.out.println();
                String answer = userInput.getUserInput();
                if (answer.isEmpty() || answer.equals("t")) {

                    MyFileWriter.writeToFileCutPrograms(cutPrograms);
                    System.out.println(MyIStrings.WRITED);
                } else mCase2();
                System.out.println();
                break;

            case 2:
                List<CutPrograms> cutProgramsList = new ArrayList<>();
                System.out.println(MyIStrings.INSERT_PROGRAM_NAME);
                String pName = Case2.userInput.getUserInput();
                ReadFromFile readFromFile = new ReadFromFile();
                cutProgramsList.addAll(readFromFile.mSearchForProgram(pName));
                if (cutProgramsList.isEmpty()) {
                    System.out.println("Nie ma takiego programu w bazie");
                    System.out.println();
                } else {
                    for (CutPrograms aCutProgramsList : cutProgramsList) {
                        System.out.println(aCutProgramsList);
                    }
                    System.out.println();
                }
                break;

            case 3:
                Map<String, CutPrograms> programsMap = getProgramsFromFile();

                System.out.println("Podaj nazwe programu do edycji: ");
                String progToEdit = userInput.getUserInput();

                if (programsMap.containsKey(progToEdit)) {
                    System.out.println();
                    System.out.println("Chcesz edytować: ");
                    System.out.println(programsMap.get(progToEdit));
                    System.out.println();
                    System.out.println("Zgadza sie?\t't lub ENTER' \\ 'n'");
                    String s = userInput.getUserInput();
                    if (s.equalsIgnoreCase("t") || s.isEmpty()) {
                        cutPrograms = new CutPrograms();
                        listOfElements = new ArrayList<>();
                        System.out.println(MyIStrings.INSERT_PROGRAM_NAME);
                        cutPrograms.setProgramName(userInput.getUserInput());
                        System.out.println(MyIStrings.INS_SHEET_DIMENSION);
                        cutPrograms.setSheetDimensions(userInput.getUserInput());
                        System.out.println(MyIStrings.INS_THICKNESS);
                        cutPrograms.setSheetThicknes(userInput.getUserInput());
                        System.out.println(MyIStrings.INS_DATE_OF_PR_CREATION);
                        cutPrograms.setDateOfGeneration(userInput.getUserInput());
                        System.out.println(MyIStrings.INSERT_No_OF_ELEMENTS);
                        ConsoleMain.noOfElements = mCheckChoice();

                        for (int i = 0; i < ConsoleMain.noOfElements; i++) {
                            ElementCutOldVer e = new ElementCutOldVer();
                            int i1 = 1;
                            System.out.println("Podaj nazwe " + (i + i1) + " elementu: ");
                            e.setPartNumber(userInput.getUserInput());
                            System.out.println(MyIStrings.INS_PART_AMOUNT);
                            e.setPartAmount(mCheckChoice());
//            System.out.println(INS_PART_DESCR);
                            e.setDescription("");
                            listOfElements.add(e);
                        }
                        cutPrograms.setElementCutted(listOfElements);
                        System.out.println("Sprawdź poprawność danych\t \"t\" lub ENTER\\\"n\"");
                        System.out.println("---------------------------------------------------");
                        System.out.println(cutPrograms);
                        System.out.println("---------------------------------------------------");
                        System.out.println();
                        String t = userInput.getUserInput();
                        if (t.isEmpty() || t.equals("t")) {
                            programsMap.replace(progToEdit, cutPrograms);
                            writeMapToFile(programsMap);
                            System.out.println(MyIStrings.WRITED);
                        }
                    } else {
                        System.out.println(MyIStrings.SOMETHING_WENT_WRONG);
                        mCase2();
                    }
                } else {
                    System.out.println(MyIStrings.UPS);
                    mCase2();
                }


                break;
            case 4:
                Map<String, CutPrograms> map = getProgramsFromFile();

                System.out.println("Podaj nazwę programu do usunięcia:");
                String inputPName = Case2.userInput.getUserInput();
                if (map.containsKey(inputPName)) {
                    map.remove(inputPName);
                    System.out.println();
                    System.out.println("Program '" + inputPName + "' został usunięty.");
                    writeMapToFile(map);
                } else {
                    System.out.println("Coś poszło nie tak podczas usuwania z bazy.\nMoże nie ma takiego programu.");
                }
                break;

            case 0:
                break;
        }
    }

    private static void writeMapToFile(Map<String, CutPrograms> map) {
        StringBuilder sb = new StringBuilder();
        Gson gson = new Gson();
        gson.getAdapter(CutPrograms.class);
        for (String s : map.keySet()) {
            sb.append(gson.toJson(map.get(s)));
            sb.append("\n");
        }
        MyFileWriter.writeToFile(sb.toString());
    }

    private static Map<String, CutPrograms> getProgramsFromFile() {
        Map<String, CutPrograms> map = new HashMap<>();
        AntiDoublePrograms adp = new AntiDoublePrograms();
        map.putAll(adp.mToMap());
        return map;
    }


    private static Integer mCheckChoice() {
        double temp = -1;
        try {
            temp = Double.valueOf(userInput.getUserInput());
        } catch (NumberFormatException e) {
            System.out.println(MyIStrings.WTF);
        }
        return (int) temp;
    }
}