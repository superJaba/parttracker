package consolCases;

import DAO.ReadFromFile;


class Case4 implements MyIStrings{

    static void mCase4(ReadFromFile readFromFile) {
        System.out.println(CHOSE);
        System.out.println(CASE4_UNDER_MENU);
        int t = ConsoleMain.mCheckChoice();
        if (t == 1) {
            readFromFile.readAllPrograms();
        } else if (t == 2) {
            System.out.println(INS_PART_NEEDED);
            String eName = ConsoleMain.userInput.getUserInput();
            readFromFile.searchForEleInPrograms(eName);
        } else {
            System.out.println(WRONG_OPERATION_CHOICE);
            mCase4(readFromFile);
        }
    }
}