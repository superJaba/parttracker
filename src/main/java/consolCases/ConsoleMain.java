package consolCases;

import DAO.ReadFromFile;
import DAO.UserInput;


/**
 * Created by Karamba on 2017-09-25
 */
public class ConsoleMain implements MyIStrings {

    static Integer noOfElements;
    static String pathToFIles;
    static UserInput userInput = new UserInput();
    private static Integer choice;
    private static boolean flag = true;
    private static ReadFromFile readFromFile;

    private static ICase iCase = new AllCases();

    public static void main(String[] args) {

        mReadConfig();

        //Menu
        choice = mMenuChose();

        while (flag) {

            if (choice < 0 || choice > 6) {
                System.out.println(WRONG_OPERATION_CHOICE);
                mMenuChose();
            }

            switch (choice) {
                case 1: //dodaj wyciety element
                    iCase.mCase1();
//                    consolCases.Case1.mCase1();
                    break;
                case 2: //opcje programow (submenu)
                    iCase.mCase2();
                    break;
                case 3: //wypisanie wycietych elementow lub konkretny element
                    System.out.println(CHOSE);
                    readFromFile = new ReadFromFile();
                    iCase.mCase3(readFromFile);
                    break;
                case 4: //przeszukanie programow pod katem elementu lub wszystkie programy
                    readFromFile = new ReadFromFile();
                    iCase.mCase4(readFromFile);
                    break;
                case 5: //laduje elementy z programu * liczba blach
                    readFromFile = new ReadFromFile();
                    iCase.mCase5(readFromFile);
                    break;
                case 6: //dodawanie zlecenia produkcyjnego
                    iCase.mCase6();
                    break;
            }
            mMenuChose();
        }
    }


    public static String mReadConfig() {
        readFromFile = new ReadFromFile();
        return pathToFIles = readFromFile.readConfigFromFile();
    }

    public static Integer mMenuChose() {
        String sOptions = MENU;
        System.out.println(sOptions);
        mCheckChoice();
        if (choice.equals("")) {
            System.out.println(WRONG_OPERATION_CHOICE);
            System.out.println(sOptions);
        }
        if (choice == 0) {
            flag = false;
        }
        return choice;
    }

   public static Integer mCheckChoice() {
        double temp = -1;
        try {
            temp = Double.valueOf(userInput.getUserInput());
        } catch (NumberFormatException e) {
            System.out.println(WTF);
        }
        return choice = (int) temp;
    }
}
