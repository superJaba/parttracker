package consolCases;

import DAO.ReadFromFile;

public interface ICase {
    void mCase1();

    void mCase2();

    void mCase3(ReadFromFile readFromFile);

    void mCase4(ReadFromFile readFromFile);

    void mCase5(ReadFromFile readFromFile);

    void mCase6();
}
