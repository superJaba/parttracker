package consolCases;

import DAO.MyFileWriter;
import model.ControlCart;

import java.util.ArrayList;
import java.util.List;

class Case6 implements MyIStrings {
    static void mCase6() {
        ControlCart cc = new ControlCart();
        System.out.print("Podaj nr zlecenia: ");
        cc.setOrderNo(ConsoleMain.userInput.getUserInput());
        System.out.print("Podaj date wykonania: ");
        cc.setCompletionDate(ConsoleMain.userInput.getUserInput());
        System.out.print("Podaj nr rysunku detalu: ");
        cc.setDrawingNumber(ConsoleMain.userInput.getUserInput());
        System.out.print("Podaj do jakiego elementu: ");
        cc.setToDetail(ConsoleMain.userInput.getUserInput());
        System.out.print("Podaj nazwe klienta: ");
        cc.setCustomer(ConsoleMain.userInput.getUserInput());
        System.out.print("Podaj gatunek materialu: ");
        cc.setSteelGrade(ConsoleMain.userInput.getUserInput());
        System.out.print("Podaj grubosc materialu: ");
        cc.setMaterialThicknes(ConsoleMain.userInput.getUserInput());
        System.out.print("Podaj ilosc detali: ");
        cc.setOrderedQuantity(Integer.valueOf(ConsoleMain.userInput.getUserInput()));
        System.out.print("Podaj ewentualne uwagi: ");
        cc.setCriticalPoints(ConsoleMain.userInput.getUserInput());
        System.out.print("Produkcja seryjna?: \'t\' lub \'ENTER\' \\ \'n\' ");
        String isSerial = ConsoleMain.userInput.getUserInput();
        if (isSerial.equals("t") || isSerial.isEmpty()) {
            cc.setSerialProduction(true);
        } else cc.setSerialProduction(false);
        System.out.println("Podaj liczbe dziełow: ");
        int noOfPositions = Integer.valueOf(ConsoleMain.mCheckChoice());
        System.out.println("Podaj nazwy dzialow i zatwierdz \'ENTER\'");
        List<String> positions = new ArrayList<>();
        for (int i = 0; i < noOfPositions; i++) {
            positions.add(ConsoleMain.userInput.getUserInput());
        }
        cc.setWorkPosition(positions);
        MyFileWriter.writeControlCartToFile(cc);

        System.out.println(cc.toString());
        System.out.println(WRITED);
    }
}