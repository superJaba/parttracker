import DAO.ReadFromFile;
import model.CutPrograms;
import model.CutProgramsDB;
import model.ElementCutForProgramsDB;
import model.ElementCutOldVer;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import util.HibernateUtil;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class InsertProgramsToDB {
    public static void main(String[] args) {

        SessionFactory factory = HibernateUtil.getSessionFactory();
        Session session = factory.getCurrentSession();
        session.beginTransaction();

        ReadFromFile rff = new ReadFromFile();
        List<CutPrograms> cutProgramsList = rff.readAllProgramsToList();
        SimpleDateFormat spdf = new SimpleDateFormat("DD-MM-YYYY");

        for (CutPrograms aCutProgramsList : cutProgramsList) {
            CutProgramsDB cutProgramsDB = new CutProgramsDB();

            cutProgramsDB.setProgramName(aCutProgramsList.getProgramName());
            cutProgramsDB.setSheetDimensions(aCutProgramsList.getSheetDimensions());
            cutProgramsDB.setSheetThicknes(aCutProgramsList.getSheetThicknes());
            try {
                cutProgramsDB.setDateOfGeneration(spdf.parse(aCutProgramsList.getDateOfGeneration()));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            List<ElementCutForProgramsDB> elementDBList = new ArrayList<>();
            for (ElementCutOldVer elementCut : aCutProgramsList.getElementCutted()) {
                ElementCutForProgramsDB elementDB = new ElementCutForProgramsDB();
                elementDB.setPartNumber(elementCut.getPartNumber());
                elementDB.setPartAmount(elementCut.getPartAmount());
                elementDB.setMachine("SpaceGear 2.5kW");
                elementDB.setCutProgramsDB(cutProgramsDB);
                elementDBList.add(elementDB);
            }
            cutProgramsDB.setElementCutList(elementDBList);
            cutProgramsDB.setMaschine("SpaceGear 2.5kW");

            session.save(cutProgramsDB);
        }
        session.getTransaction().commit();
        session.close();
    }
}
