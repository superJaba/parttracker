import DAO.DBImp;
import model.CutProgramsDB;
import model.ElementCutForProgramsDB;

import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class DBTest {
    public static void main(String[] args) {

        DBImp db = new DBImp();

//        for (int i = 0; i < 1000; i++) {
//            Random random = new Random(1000);
//            ec.setPartNumber("test" + i);
//            ec.setPartAmount(random.nextInt());
//            ec.setDateOfCut(LocalDateTime.now().toString());
//            ec.setDescription("test bazy");
////            ec.set("SpaceGear 2.5kW");
//            db.save(ec);
//        }
//        User user = new User();
//        user.setUserLogin("root");
//        user.setUserPassword("1101");
//        user.setUserGroup("superAdmin");
//        user.setFirstName("Sebastian");
//        user.setLastName("Piechowski");
//        user.setUserEmail("s@wp.pl");
//        db.save(user);

        CutProgramsDB cutProgramsDB = new CutProgramsDB();
        cutProgramsDB.setProgramName("01");
        cutProgramsDB.setSheetDimensions("2500 x 1250");
        cutProgramsDB.setSheetThicknes("4 FE");
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat();
        cutProgramsDB.setDateOfGeneration(Date.from(Instant.now()));
        List<ElementCutForProgramsDB> listElementForProgramsDB = new ArrayList<>();
        ElementCutForProgramsDB elementCutForProgramsDB = new ElementCutForProgramsDB("7", 100, "Sace Gear 2.5kW");
        elementCutForProgramsDB.setCutProgramsDB(cutProgramsDB);
        ElementCutForProgramsDB element1 = new ElementCutForProgramsDB("8", 1001, "Sace Gear 2.5kW");
        element1.setCutProgramsDB(cutProgramsDB);
//        ElementCutForProgramsDB ele2 = new ElementCutForProgramsDB("9", 1002, "Sace Gear 2.5kW");
//        ele2.setCutProgramsDB(cutProgramsDB);
        listElementForProgramsDB.add(elementCutForProgramsDB);
        listElementForProgramsDB.add(element1);
//        listElementForProgramsDB.add(ele2);
//        listElementForProgramsDB.add(ele3);

        cutProgramsDB.setElementCutList(listElementForProgramsDB);
        System.out.println(cutProgramsDB.toString());
        db.save(cutProgramsDB);


    }
}
