import DAO.ReadFromFile;
import model.ElementCut;
import model.ElementCutOldVer;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import util.HibernateUtil;

import java.util.ArrayList;
import java.util.List;

public class ElementTestHibernate {
    public static void main(String[] args) {

        SessionFactory factory = HibernateUtil.getSessionFactory();
        Session session = factory.getCurrentSession();
        session.beginTransaction();


        ReadFromFile rff = new ReadFromFile();
        List<ElementCutOldVer> list = new ArrayList<>(rff.readAllElementsToList());

        for (ElementCutOldVer aList : list) {
            ElementCut elementCut = new ElementCut();
            elementCut.setPartNumber(aList.getPartNumber());
            elementCut.setPartAmount(aList.getPartAmount());
            elementCut.setDateOfCut(aList.getDateOfCut());
            elementCut.setDescription(aList.getDescription());
            elementCut.setMachine("SpaceGear 2.5kW");
            session.save(elementCut);
        }
        session.getTransaction().commit();
        session.close();
    }
}
